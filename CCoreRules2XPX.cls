VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCoreRules2XPX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Opens Core Rules 2.0 Expansion export files

Private Type CEncTemplate

End Type

Private Type CEncTemplateMonsterPropertiesOb

End Type


Private Type CMonsterTypeOb
  ' 01 80 is the start of a new record
  bytNameLen As Byte
  strMonsterName As String
  bytImageListIndex As Byte ' should be 00
  bytFileNameLen As Byte
  strFileName As String
  bytPathLen As Byte
  strPathName As String
  bytLongFileNameLen As Byte
  strLongFileName As String
End Type

Private Type CClassOb

End Type

Private Type CMonsterOb

End Type

Private Type CSpellsOb

End Type

Private Type CClassesAllowedOb

End Type


Private Type CClassesAllowedHelperOb

End Type


Private Type CEncounterFreqTableOb

End Type
