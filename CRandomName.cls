VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRandomName"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarstrName As String 'local copy
Private mvarstrSourceFolder As String 'local copy
Private mvarstrFileName As String 'local copy
Public Property Let strFileName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strFileName = 5
    mvarstrFileName = vData
End Property


Public Property Get strFileName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strFileName
    strFileName = mvarstrFileName
End Property



Public Property Let strSourceFolder(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSourceFolder = 5
    mvarstrSourceFolder = vData
End Property


Public Property Get strSourceFolder() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSourceFolder
    strSourceFolder = mvarstrSourceFolder
End Property



Public Property Let strName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strName = 5
    mvarstrName = vData
End Property


Public Property Get strName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strName
    strName = mvarstrName
End Property


Public Function RandomName(ByVal strGender1 As String) As String
  
  Dim strTemp As String
  Dim fso As Object, ts As Object
  Dim strParsed() As String
  Dim strNameTemplate() As String
  Dim strSearch As String
  Dim strActiveSection As String
  Dim strActiveSubsection As String
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim srtNameList() As String
  Dim strFinalName As String
  Dim NameTable As New CStringTable
  Dim strNextSection As String
  Dim ChildFile As New CRandomName
  Dim strtempp() As String
  
  i = d100
  strFinalName = ""
  If FileExist(mvarstrSourceFolder & "\" & mvarstrFileName) = True Then
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set ts = fso.OpenTextFile(mvarstrSourceFolder & "\" & mvarstrFileName)
    Do
      strTemp = ts.Readline
      If Len(Trim(strTemp)) > 0 Then
        strParsed = Split(strTemp, Chr(9))
        If strParsed(0) Like "*[RULES]*" Then
          Do
            strTemp = ts.Readline
            If Len(Trim(strTemp)) > 0 Then
              strParsed = Split(strTemp, Chr(9))
              If IsNumeric(Trim(strParsed(0))) = True Then
                If CLng(strParsed(0)) >= i Then
                  Exit Do
                End If
              End If
            End If
          Loop Until strParsed(0) Like "[*]" Or ts.AtEndOfStream = True
          strNameTemplate = Split(strTemp, Chr(9))
          Exit Do
        End If
      End If
    Loop Until ts.AtEndOfStream = True
    
    'Debug.Print strTemp
    
    ' Now go back to the beginning of the file and find the strHeader
    ts.Close
    
    For i = 1 To UBound(strNameTemplate)
      strActiveSection = LEFT(strNameTemplate(i), Len(strNameTemplate(i)) - 1)
      strActiveSection = Right(strActiveSection, Len(strActiveSection) - 1)
      
      If strActiveSection Like "{*}" Then
        Set ChildFile = New CRandomName
        ChildFile.strSourceFolder = mvarstrSourceFolder
        ChildFile.strFileName = LEFT(strActiveSection, Len(strActiveSection) - 1)
        ChildFile.strFileName = Right(ChildFile.strFileName, Len(ChildFile.strFileName) - 1) & ".nam"
        'Debug.Print ChildFile.strFileName
        strFinalName = strFinalName & ChildFile.RandomName(strGender1)
      
      Else
      
        If i < UBound(strNameTemplate) Then
          strNextSection = strNameTemplate(i + 1)
        Else
          strNextSection = "ENDENDEND"
        End If
        
        Set ts = fso.OpenTextFile(mvarstrSourceFolder & "\" & mvarstrFileName)
        Do
          strTemp = ts.Readline
        Loop Until strTemp Like "[[]" & strActiveSection & "[]]" Or ts.AtEndOfStream = True
        
        ' now read it in line by line and compile a list of strings
        strActiveSubsection = ""
        Set NameTable = New CStringTable
        Do
          strTemp = ts.Readline
          
          Select Case strTemp
            Case "[PREGENDER:M]"
              strActiveSubsection = "M"
            Case "[PREGENDER:F]"
              strActiveSubsection = "F"
            Case "[/PRE]"
              strActiveSubsection = ""
            Case strNextSection
              Exit Do
            Case Else
              If Len(strTemp) = 0 Then Exit Do

              If Not (strTemp Like "[#]*") And (strActiveSubsection = "" Or _
                (strActiveSubsection = "M" And UCase(LEFT(strGender1, 1)) Like "M") Or _
                (strActiveSubsection = "F" And UCase(LEFT(strGender1, 1)) Like "F")) Then
                
                If InStr(strTemp, "#") > 0 Then
                  strTemp = LEFT(strTemp, InStr(strTemp, "#") - 1)
                End If
                NameTable.Add strTemp
              'Else
                'Exit Do
              End If
          End Select
          
        Loop Until ts.AtEndOfStream = True
        
        If NameTable.Count > 0 Then
          k = dN(1, NameTable.Count)
          strFinalName = strFinalName & NameTable(k).Value
        Else
          strFinalName = ""
        End If
        
        ts.Close
      End If
    Next i
    
    'ts.Close
    Set ts = Nothing
    
    If mvarstrFileName Like "*drow*" Then
      ' replace all "-" with " "
      strtempp = Split(strFinalName, "-")
      strFinalName = ""
      For i = 0 To UBound(strtempp)
        strFinalName = strFinalName & strtempp(i) & " "
      Next i
    End If
    
    RandomName = Trim(strFinalName)
  Else
    MsgBox "File " & mvarstrSourceFolder & "\" & mvarstrFileName & " is missing.", vbCritical
    RandomName = ""
  End If
  Set fso = Nothing
End Function

Public Function WeirdName() As String
  Dim strOut As String
  Dim lngVolCons As Long
  Dim lngRoll As Long
  Dim lnglen As Long
  Dim i As Long
  
  strOut = ""
  lnglen = d20
  
  For i = 1 To lnglen
    lngVolCons = d6
    Select Case lngVolCons
      Case 1 To 4
        lngRoll = d20
        Select Case lngRoll
          Case 1
            strOut = strOut & "b"
          Case 2
            strOut = strOut & "c"
          Case 3
            strOut = strOut & "d"
          Case 4
            strOut = strOut & "f"
          Case 5
            strOut = strOut & "g"
          Case 6
            strOut = strOut & "h"
          Case 7
            strOut = strOut & "j"
          Case 8
            strOut = strOut & "k"
          Case 9
            strOut = strOut & "l"
          Case 10
            strOut = strOut & "m"
          Case 11
            strOut = strOut & "n"
          Case 12
            strOut = strOut & "p"
          Case 13
            strOut = strOut & "q"
          Case 14
            strOut = strOut & "r"
          Case 15
            strOut = strOut & "s"
          Case 16
            strOut = strOut & "t"
          Case 17
            strOut = strOut & "v"
          Case 18
            strOut = strOut & "w"
          Case 19
            strOut = strOut & "x"
          Case 20
            strOut = strOut & "z"
        End Select
        
      Case 5 To 6
        lngRoll = d6
        Select Case lngRoll
          Case 1
            strOut = strOut & "a"
          Case 2
            strOut = strOut & "e"
          Case 3
            strOut = strOut & "i"
          Case 4
            strOut = strOut & "o"
          Case 5
            strOut = strOut & "u"
          Case 6
            strOut = strOut & "y"
        End Select
    End Select
  Next i
  strOut = UCase(LEFT(strOut, 1)) & Mid(strOut, 2)
  WeirdName = strOut
  
End Function
