VERSION 5.00
Begin VB.Form frmFeatChoice 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Feat Choice"
   ClientHeight    =   5520
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9480
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   9480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8520
      TabIndex        =   8
      Top             =   5040
      Width           =   915
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove"
      Enabled         =   0   'False
      Height          =   315
      Left            =   4320
      TabIndex        =   5
      Top             =   4200
      Width           =   855
   End
   Begin VB.ListBox lstSelectedFeats 
      Height          =   1620
      Left            =   60
      TabIndex        =   3
      Top             =   2520
      Width           =   9375
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   315
      Left            =   4313
      TabIndex        =   2
      Top             =   1800
      Width           =   855
   End
   Begin VB.ListBox lstAvailableFeats 
      Height          =   1425
      Left            =   60
      TabIndex        =   0
      Top             =   300
      Width           =   9375
   End
   Begin VB.Label lblRemainingSelections 
      Caption         =   "1"
      Height          =   255
      Left            =   2460
      TabIndex        =   7
      Top             =   5100
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "Number of selections remaining:"
      Height          =   195
      Left            =   180
      TabIndex        =   6
      Top             =   5100
      Width           =   2250
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Selected"
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   2280
      Width           =   9345
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Available"
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   9345
   End
End
Attribute VB_Name = "frmFeatChoice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

