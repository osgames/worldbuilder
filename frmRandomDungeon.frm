VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmRandomDungeon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Random Dungeon Generator"
   ClientHeight    =   7425
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8985
   Icon            =   "frmRandomDungeon.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   8985
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1020
      Top             =   6720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6135
      Left            =   1380
      TabIndex        =   8
      Top             =   840
      Width           =   7545
      _ExtentX        =   13309
      _ExtentY        =   10821
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Dungeon Overview"
      TabPicture(0)   =   "frmRandomDungeon.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame7"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtMonsterEncounters"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtTotalTraps"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Room Overview"
      TabPicture(1)   =   "frmRandomDungeon.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(1)=   "Frame6"
      Tab(1).ControlCount=   2
      TabCaption(2)   =   "Hidden Items"
      TabPicture(2)   =   "frmRandomDungeon.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(1)=   "Frame2"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Inhabitants"
      TabPicture(3)   =   "frmRandomDungeon.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame8"
      Tab(3).Control(1)=   "Frame5"
      Tab(3).ControlCount=   2
      Begin VB.TextBox txtTotalTraps 
         Height          =   315
         Left            =   4200
         TabIndex        =   26
         Text            =   "0"
         Top             =   2760
         Width           =   1035
      End
      Begin VB.TextBox txtMonsterEncounters 
         Height          =   315
         Left            =   1800
         TabIndex        =   24
         Text            =   "0"
         Top             =   2760
         Width           =   1035
      End
      Begin VB.Frame Frame7 
         Caption         =   "Total Treasure"
         Height          =   2235
         Left            =   180
         TabIndex        =   21
         Top             =   420
         Width           =   7095
         Begin VB.ListBox lstAllTreasure 
            Height          =   1815
            Left            =   180
            TabIndex        =   22
            Top             =   300
            Width           =   6735
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Doors"
         Height          =   1275
         Left            =   -72420
         TabIndex        =   19
         Top             =   480
         Width           =   4815
         Begin VB.ListBox lstDoors 
            Height          =   840
            Left            =   180
            TabIndex        =   20
            Top             =   300
            Width           =   4515
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Features and furnishings"
         Height          =   3315
         Left            =   -74880
         TabIndex        =   17
         Top             =   480
         Width           =   2415
         Begin VB.ListBox lstFeatures 
            Height          =   2790
            ItemData        =   "frmRandomDungeon.frx":01BA
            Left            =   120
            List            =   "frmRandomDungeon.frx":01C1
            TabIndex        =   18
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Hidden Treasure"
         Height          =   2055
         Left            =   -74820
         TabIndex        =   15
         Top             =   360
         Width           =   6975
         Begin VB.ListBox lstHiddenTreasure 
            Height          =   1620
            Left            =   120
            TabIndex        =   16
            Top             =   240
            Width           =   6675
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Traps"
         Height          =   2715
         Left            =   -74820
         TabIndex        =   13
         Top             =   2520
         Width           =   6975
         Begin VB.ListBox lstTraps 
            Height          =   2205
            Left            =   180
            TabIndex        =   14
            Top             =   300
            Width           =   6615
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Monsters encountered"
         Height          =   1395
         Left            =   -74820
         TabIndex        =   11
         Top             =   420
         Width           =   7215
         Begin VB.ListBox lstMonsterEncounter 
            Height          =   840
            Left            =   120
            TabIndex        =   12
            Top             =   360
            Width           =   6975
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Monster Treasure"
         Height          =   3435
         Left            =   -74880
         TabIndex        =   9
         Top             =   1920
         Width           =   7215
         Begin VB.ListBox lstMonsterTreasure 
            Height          =   2790
            Left            =   180
            TabIndex        =   10
            Top             =   240
            Width           =   6915
         End
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Traps:"
         Height          =   195
         Left            =   3720
         TabIndex        =   25
         Top             =   2820
         Width           =   450
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Monster encounters:"
         Height          =   195
         Left            =   300
         TabIndex        =   23
         Top             =   2820
         Width           =   1455
      End
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   255
      Left            =   180
      TabIndex        =   7
      Top             =   7080
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.ListBox lstRooms 
      Height          =   6105
      Left            =   60
      TabIndex        =   6
      Top             =   840
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Inputs"
      Height          =   675
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8655
      Begin VB.CommandButton cmdExport 
         Caption         =   "Export"
         Height          =   375
         Left            =   7680
         TabIndex        =   27
         Top             =   180
         Width           =   795
      End
      Begin VB.CommandButton cmdRoll 
         Caption         =   "Roll"
         Height          =   375
         Left            =   6720
         TabIndex        =   5
         Top             =   180
         Width           =   855
      End
      Begin VB.TextBox txtDungeonLevel 
         Height          =   285
         Left            =   3540
         TabIndex        =   4
         Text            =   "1"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtRoomCount 
         Height          =   285
         Left            =   1500
         TabIndex        =   2
         Text            =   "30"
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Dungeon Level:"
         Height          =   195
         Left            =   2340
         TabIndex        =   3
         Top             =   285
         Width           =   1140
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Number of rooms:"
         Height          =   195
         Left            =   180
         TabIndex        =   1
         Top             =   285
         Width           =   1245
      End
   End
End
Attribute VB_Name = "frmRandomDungeon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private db As CDB
Private Rooms As New CDungeonRooms

Private lngNumRooms As Long
Private lngDungeonLevel As Long



Private Sub cmdExport_Click()
  Dim strFilename As String
  Dim strpathname As String
  
  With CommonDialog1
    .Filter = "Text (*.txt)|*.txt"
    
    .ShowSave
    If Len(.filename) > 0 Then
      WriteReport .filename
    End If
  End With
  
End Sub

Private Sub cmdRoll_Click()
  Dim lngRoomID As Long
  Dim lngContentInx As Long
  Dim strTableResult As String
  Dim strParsedTableResult() As String
  Dim strFeatures() As String
  Dim bytFeatureInx As Byte
  Dim lngTrapCR As Long
  Dim lngEncounterRoll1 As Long
  Dim lngEncounterRoll2 As Long
  Dim objTreasure As New CTreasureHoard
  
  Dim lngAppropriateMinEL As Single
  Dim lngAppropriateMaxEL As Single
  
  lngNumRooms = CLng(txtRoomCount.Text)
  lngDungeonLevel = CLng(txtDungeonLevel.Text)
  Set Rooms = New CDungeonRooms
  lstRooms.Clear
  
  ProgressBar1.Value = ProgressBar1.min
  ProgressBar1.max = lngNumRooms
  
  For lngRoomID = 1 To lngNumRooms
    Rooms.Add
    lstRooms.AddItem "Room " & CStr(lngRoomID)
    
    strTableResult = RollTable("RoomContents")
    strParsedTableResult = Split(strTableResult, "|")
    For lngContentInx = 0 To UBound(strParsedTableResult)
      If strParsedTableResult(lngContentInx) Like "1d4 Minor Features*" Then
        Rooms(Rooms.Count).bytNumMinorFeatures = ceil(Rnd * 4)
        ReDim strFeatures(Rooms(Rooms.Count).bytNumMinorFeatures - 1) As String
        For bytFeatureInx = 0 To Rooms(Rooms.Count).bytNumMinorFeatures - 1
          strFeatures(bytFeatureInx) = RollTable("Minor Features")
        Next bytFeatureInx
        Rooms(Rooms.Count).SetMinorFeatures strFeatures
      End If
      If strParsedTableResult(lngContentInx) Like "*1d4 Major Features" Then
        Rooms(Rooms.Count).bytNumMajorFeatures = ceil(Rnd * 4)
        ReDim strFeatures(Rooms(Rooms.Count).bytNumMajorFeatures - 1) As String
        For bytFeatureInx = 0 To Rooms(Rooms.Count).bytNumMajorFeatures - 1
          strFeatures(bytFeatureInx) = RollTable("Major Features")
        Next bytFeatureInx
        Rooms(Rooms.Count).SetMajorFeatures strFeatures
      End If
      
      If strParsedTableResult(lngContentInx) Like "*trap*" Then
        lngTrapCR = ceil(Rnd * 4) + ceil(Rnd * 4) - 5 + lngDungeonLevel
        If lngTrapCR <= 3 Then
          Rooms(Rooms.Count).strTrapName = RollTable("Traps CR 1-3")
        Else
          Rooms(Rooms.Count).strTrapName = RollTable("Traps CR 4+")
        End If
      End If
      
      If strParsedTableResult(lngContentInx) Like "Monster*" Then
      
        ' calculate the appropriate EL
        lngEncounterRoll1 = d100
        Select Case lngEncounterRoll1
          Case 1 To 50 ' 50% below encounter level
            lngAppropriateMinEL = lngDungeonLevel - 5
            lngAppropriateMaxEL = lngDungeonLevel - 1
          Case 51 To 75 ' 25% challenging
            lngAppropriateMinEL = lngDungeonLevel - 1
            lngAppropriateMaxEL = lngDungeonLevel + 1
          Case 76 To 75 ' 10% hard
            lngAppropriateMinEL = lngDungeonLevel + 1
            lngAppropriateMaxEL = lngDungeonLevel + 2
          Case 76 To 85 ' 10% really hard
            lngAppropriateMinEL = lngDungeonLevel + 2
            lngAppropriateMaxEL = lngDungeonLevel + 3
          Case 96 To 100
            lngAppropriateMinEL = lngDungeonLevel + 3
            lngAppropriateMaxEL = lngDungeonLevel + 5
        End Select
        If lngAppropriateMinEL < 0 Then lngAppropriateMinEL = 0
        If lngAppropriateMaxEL < 1 Then lngAppropriateMaxEL = 1
        
      
        ' roll a random monster encounter with an appropriate CR
        lngEncounterRoll1 = ceil(Rnd * 100)
        If lngEncounterRoll1 <= 65 Then
          db.OpenDB "SELECT EncounterName, MinEL, MaxEL FROM MonsterEncounter WHERE Frequency " & _
            "LIKE 'Common' AND AvgEL >= " & CStr(lngAppropriateMinEL) & _
            " AND AvgEL <= " & CStr(lngAppropriateMaxEL) & " ORDER BY EncounterName"
          If db.rs.State > 0 Then
            If db.rs.RecordCount > 0 Then
              lngEncounterRoll2 = ceil(Rnd * db.rs.RecordCount)
              db.rs.MoveFirst
              db.rs.move lngEncounterRoll2 - 1
              Rooms(Rooms.Count).strEncounterName = db.rs!EncounterName ' & " (EL " & Format(db.rs!minEL, "0") & " to " & Format(db.rs!maxEL, "0") & ")"
            End If
            db.CloseDB
          End If
        ElseIf lngEncounterRoll1 <= 85 Then
          db.OpenDB "SELECT EncounterName, MinEL, MaxEL FROM MonsterEncounter WHERE Frequency " & _
            "LIKE 'Uncommon' AND AvgEL >= " & CStr(lngAppropriateMinEL) & _
            " AND AvgEL <= " & CStr(lngAppropriateMaxEL) & " ORDER BY EncounterName"
          If db.rs.State > 0 Then
            If db.rs.RecordCount > 0 Then
              lngEncounterRoll2 = ceil(Rnd * db.rs.RecordCount)
              db.rs.MoveFirst
              db.rs.move lngEncounterRoll2 - 1
              Rooms(Rooms.Count).strEncounterName = db.rs!EncounterName '& " (EL " & Format(db.rs!minEL, "0") & " to " & Format(db.rs!maxEL, "0") & ")"
            End If
            db.CloseDB
          End If
        ElseIf lngEncounterRoll1 <= 96 Then
          db.OpenDB "SELECT EncounterName, MinEL, MaxEL FROM MonsterEncounter WHERE Frequency " & _
            "LIKE 'Rare' AND AvgEL >= " & CStr(lngAppropriateMinEL) & _
            " AND AvgEL <= " & CStr(lngAppropriateMaxEL) & " ORDER BY EncounterName"
          If db.rs.State > 0 Then
            If db.rs.RecordCount > 0 Then
              lngEncounterRoll2 = ceil(Rnd * db.rs.RecordCount)
              db.rs.MoveFirst
              db.rs.move lngEncounterRoll2 - 1
              Rooms(Rooms.Count).strEncounterName = db.rs!EncounterName '& " (EL " & Format(db.rs!minEL, "0") & " to " & Format(db.rs!maxEL, "0") & ")"
            End If
            db.CloseDB
          End If
        Else
          db.OpenDB "SELECT EncounterName, MinEL, MaxEL FROM MonsterEncounter WHERE Frequency " & _
            "LIKE 'Very Rare' AND AvgEL >= " & CStr(lngAppropriateMinEL) & _
            " AND AvgEL <= " & CStr(lngAppropriateMaxEL) & " ORDER BY EncounterName"
          If db.rs.State > 0 Then
            If db.rs.RecordCount > 0 Then
              lngEncounterRoll2 = ceil(Rnd * db.rs.RecordCount)
              db.rs.MoveFirst
              db.rs.move lngEncounterRoll2 - 1
              Rooms(Rooms.Count).strEncounterName = db.rs!EncounterName '& " (EL " & Format(db.rs!minEL, "0") & " to " & Format(db.rs!maxEL, "0") & ")"
            End If
            db.CloseDB
          End If
        End If
      
        Set Rooms(Rooms.Count).MonsterEncounters = New CMonsterEncounters
        
        Rooms(Rooms.Count).MonsterEncounters.Add
        Rooms(Rooms.Count).MonsterEncounters(1).strName = Rooms(Rooms.Count).strEncounterName
        Rooms(Rooms.Count).MonsterEncounters(1).RollEncounter
        
        
        
      
      End If
        
      ' hidden treasure
      If strParsedTableResult(lngContentInx) Like "*treasure*" Then
        Set Rooms(Rooms.Count).Treasure = New CTreasureHoards
        Rooms(Rooms.Count).Treasure.Add
        Rooms(Rooms.Count).Treasure(1).sngCR = CSng(lngDungeonLevel)
        Rooms(Rooms.Count).Treasure(1).RollTreasure
      End If
    
    
    Next lngContentInx
    
    ProgressBar1.Value = lngRoomID
    DoEvents
  Next lngRoomID
  UpdateTotalTreasureList
End Sub


Private Function RollTable(strTableName As String, Optional bytDSize As Byte = 100) As String
  Dim ddb As New CDB
  Dim bytRoll As Byte
  Dim strResult As String
  
  ddb.OpenDB "SELECT * FROM DungeonTables WHERE TableName LIKE '" & _
    strTableName & "' ORDER BY MinRoll"
  
  bytRoll = ceil(Rnd * 100)
  strResult = ""
  If ddb.rs.State > 0 Then
    If ddb.rs.RecordCount > 0 Then
      ddb.rs.MoveFirst
      Do
        ddb.rs.MoveNext
        If ddb.rs.EOF = True Then Exit Do
      Loop Until ddb.rs!minRoll >= bytRoll
      ddb.rs.MovePrevious
      If ddb.rs.EOF = True Then ddb.rs.MoveLast
      
      strResult = ddb.rs!Result
      
      If Not IsNull(ddb.rs!RerollTable) Then
        strResult = strResult & "|" & RollTable(ddb.rs!RerollTable, ddb.rs!RerollDie)
      End If
    End If
    ddb.CloseDB
  End If
  RollTable = strResult
End Function

Private Sub Form_Load()
  Set db = New CDB
  Randomize Timer
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'  db.CloseDB
'End Sub

Private Sub lstRooms_Click()
  Dim lngRoomID As Long
  Dim i As Long
  
  lngRoomID = CLng(Mid(lstRooms.List(lstRooms.ListIndex), 6))
  lstFeatures.Clear
  lstHiddenTreasure.Clear
  lstMonsterEncounter.Clear
  lstTraps.Clear
  lstDoors.Clear
  
  With Rooms(lngRoomID)
    If .bytNumMajorFeatures > 0 Then
      For i = 0 To .bytNumMajorFeatures - 1
        lstFeatures.AddItem .GetMajorFeatures(i)
      Next i
    End If
    If .bytNumMinorFeatures > 0 Then
      For i = 0 To .bytNumMinorFeatures - 1
        lstFeatures.AddItem .GetMinorFeatures(i)
      Next i
    End If
    lstTraps.AddItem .strTrapName
    lstMonsterEncounter.AddItem .strEncounterName
  
    If .Treasure.Count > 0 Then
      lstHiddenTreasure.Clear
      If .Treasure(1).lngCP > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lngCP & " cp"
      If .Treasure(1).lngSP > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lngSP & " sp"
      If .Treasure(1).lnggp > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lnggp & " gp"
      If .Treasure(1).lngPP > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lngPP & " pp"
      
      If .Treasure(1).lngGems > 0 Then
        For i = 1 To .Treasure(1).objGems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objGems(i).strGemType & " worth " & .Treasure(1).objGems(i).lngValue & " gp"
        Next i
      End If
      If .Treasure(1).lngArt > 0 Then
        For i = 1 To .Treasure(1).objGems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objGems(i).strGemType & " worth " & .Treasure(1).objGems(i).lngValue & " gp"
        Next i
      End If
      
      If .Treasure(1).lngMundaneItems > 0 Then
        For i = 1 To .Treasure(1).objItems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objItems(i).strGemType & " worth " & .Treasure(1).objItems(i).lngValue & " gp"
        Next i
      End If
      If .Treasure(1).lngMinorItems > 0 Then
        For i = 1 To .Treasure(1).objItems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objItems(i).strGemType & " worth " & .Treasure(1).objItems(i).lngValue & " gp"
        Next i
      End If
      If .Treasure(1).lngMediumItems > 0 Then
        For i = 1 To .Treasure(1).objItems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objItems(i).strGemType & " worth " & .Treasure(1).objItems(i).lngValue & " gp"
        Next i
      End If
      If .Treasure(1).lngMajorItems > 0 Then
        For i = 1 To .Treasure(1).objItems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objItems(i).strGemType & " worth " & .Treasure(1).objItems(i).lngValue & " gp"
        Next i
      End If
      If .Treasure(1).lngEpicItems > 0 Then
        For i = 1 To .Treasure(1).objItems.Count
          lstHiddenTreasure.AddItem .Treasure(1).objItems(i).strGemType & " worth " & .Treasure(1).objItems(i).lngValue & " gp"
        Next i
      End If
      
      'If .Treasure(1).lngGems > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lngGems & " gems worth a total of " & CStr(.Treasure(1).lngTotalGemValue) & " gp"
      'If .Treasure(1).lngArt > 0 Then lstHiddenTreasure.AddItem .Treasure(1).lngArt & " pieces of art worth a total of " & CStr(.Treasure(1).lngTotalGemValue) & " gp"
      lstHiddenTreasure.AddItem "Total hoard value = " & CStr(.Treasure(1).lngTotalHoardValue) & " gp"
      
    End If
    
  End With
  
End Sub


Private Sub UpdateTotalTreasureList()
  Dim lngRoomID As Long
  Dim i As Long, j As Long, k As Long
  Dim AllHoards As New CTreasureHoards
  Dim lngHoardID As Long
  
  lstAllTreasure.Clear
  For lngRoomID = 1 To Rooms.Count
    
    With Rooms(lngRoomID)
      If .Treasure.Count > 0 Then
        AllHoards.Add .Treasure(1)
      End If
      If Not (.MonsterEncounters Is Nothing) Then
        If .MonsterEncounters.Count > 0 Then
          For i = 1 To .MonsterEncounters.Count
            If .MonsterEncounters(i).Data.Count > 0 Then
              For j = 1 To .MonsterEncounters(i).Data.Count
                If .MonsterEncounters(i).Data(j).MonsterStatistics.Count > 0 Then
                  For k = 1 To .MonsterEncounters(i).Data(j).MonsterStatistics.Count
                    AllHoards.Add .MonsterEncounters(i).Data(j).MonsterStatistics(k).Treasure
                  Next k
                End If
              Next j
            End If
          Next i
        End If
      End If
    End With
  Next lngRoomID
  
  AllHoards.Tabulate
  
  If AllHoards.Count > 0 Then
    If AllHoards.lngTotalCP > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalCP & " cp"
    If AllHoards.lngTotalSP > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalSP & " sp"
    If AllHoards.lngTotalGP > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalGP & " gp"
    If AllHoards.lngTotalPP > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalPP & " pp"
    
    ' loop through gems and art
    For lngRoomID = 1 To AllHoards.Count
      If AllHoards(lngRoomID).lngGems > 0 Then
        For i = 1 To AllHoards(lngRoomID).objGems.Count
          lstAllTreasure.AddItem AllHoards(lngRoomID).objGems(i).strGemType & " worth " & AllHoards(lngRoomID).objGems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngRoomID).lngArt > 0 Then
        For i = 1 To AllHoards(lngRoomID).objGems.Count
          lstAllTreasure.AddItem AllHoards(lngRoomID).objGems(i).strGemType & " worth " & AllHoards(lngRoomID).objGems(i).lngValue & " gp"
        Next i
      End If
    Next lngRoomID
      
    ' loop through magic items
    For lngHoardID = 1 To AllHoards.Count
      If AllHoards(lngHoardID).lngMundaneItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstAllTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMinorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstAllTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMediumItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstAllTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMajorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstAllTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngEpicItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstAllTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
    Next lngHoardID
    'If AllHoards.lngTotalGems > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalGems & " gems worth a total of " & CStr(AllHoards.lngTotalGemValue) & " gp"
    'If AllHoards.lngTotalArtObjects > 0 Then lstAllTreasure.AddItem AllHoards.lngTotalArtObjects & " pieces of art worth a total of " & CStr(AllHoards.lngTotalArtValue) & " gp"
    lstAllTreasure.AddItem "Total hoard value = " & CStr(AllHoards.lngTotalCoinValue + AllHoards.lngTotalArtValue + AllHoards.lngTotalGemValue + AllHoards.lngTotalItemValue) & " gp"

  End If
End Sub


Public Sub WriteReport(strFilename As String)
  Dim log As New CLogAndIniRoutines
  Dim lngRoomID As Long
  Dim i As Long, j As Long, k As Long, m As Long
  Dim RandomName As New CRandomName
  Dim strNameFileList(25) As String
  
  strNameFileList(0) = "hispanic_NAMES.nam"
  strNameFileList(1) = "mar_knightly_names.nam"
  strNameFileList(2) = "rw_african_names.nam"
  strNameFileList(3) = "rw_anglo_saxon_latinized_names.nam"
  strNameFileList(4) = "rw_anglo_saxon_names.nam"
  strNameFileList(5) = "rw_chinese_names.nam"
  strNameFileList(6) = "rw_english_late_period_names.nam"
  strNameFileList(7) = "rw_flemish_names.nam"
  strNameFileList(8) = "rw_french_names.nam"
  strNameFileList(9) = "rw_german_names.nam"
  strNameFileList(10) = "rw_gothic_names.nam"
  strNameFileList(11) = "rw_greek_names.nam"
  strNameFileList(12) = "rw_hebrew_names.nam"
  strNameFileList(13) = "rw_japanese_names.nam"
  strNameFileList(14) = "rw_native_american_names.nam"
  strNameFileList(15) = "rw_persian_names.nam"
  strNameFileList(16) = "rw_sumerian_names.nam"
  strNameFileList(17) = "rw_English_middle_Period_"
  strNameFileList(18) = "rw_frankish_"
  strNameFileList(19) = "rw_Irish_Gaelic_"
  strNameFileList(20) = "rw_Italian_Renaissance_"
  strNameFileList(21) = "rw_pictish_classical_names.nam"
  strNameFileList(22) = "rw_pictish_medieval_"
  strNameFileList(23) = "rw_roman_"
  strNameFileList(24) = "rw_russian_"
  strNameFileList(25) = "rw_scottish_gaelic_"
  
  
  
  
  
  RandomName.strFilename = "orc.nam"
  RandomName.strSourceFolder = App.Path & "\names\"
  log.strLogFileName = strFilename
  log.bWriteLog = True
  log.writeLogEntry "Dungeon created " & Format(Now, "mm/dd/yyyy hh:mm:ss")
  log.writeLogEntry ""
  log.writeLogEntry "Dungeon level: " & CStr(lngDungeonLevel)

  If Not (Rooms Is Nothing) Then
    If Rooms.Count > 0 Then
      ProgressBar1.max = Rooms.Count
      ProgressBar1.Value = ProgressBar1.min
      
      For lngRoomID = 1 To Rooms.Count
        log.writeLogEntry ""
        log.writeLogEntry "ROOM " & CStr(lngRoomID)
        If Rooms(lngRoomID).bytNumMajorFeatures > 0 Then
          log.writeLogEntry ""
          log.writeLogEntry "   Features:"
          For i = 0 To Rooms(lngRoomID).bytNumMajorFeatures - 1
            log.writeLogEntry "      " & Rooms(lngRoomID).GetMajorFeatures(i)
          Next i
        End If

      
        If Len(Rooms(lngRoomID).strTrapName) > 0 Then
          log.writeLogEntry ""
          log.writeLogEntry "   Trap:"
          log.writeLogEntry "      " & Rooms(lngRoomID).strTrapName & " (DC " & CStr(lngDungeonLevel + 20) & " to spot or disable)"
        End If
        If Not (Rooms(lngRoomID).Treasure Is Nothing) Then
          If Rooms(lngRoomID).Treasure.Count > 0 Then
            log.writeLogEntry ""
            log.writeLogEntry "   Hidden Treasure (Search DC " & CStr(lngDungeonLevel + 20) & "):"
            For i = 1 To Rooms(lngRoomID).Treasure.Count
              If Rooms(lngRoomID).Treasure(i).lngCP > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lngCP & " cp"
              If Rooms(lngRoomID).Treasure(i).lngSP > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lngSP & " sp"
              If Rooms(lngRoomID).Treasure(i).lnggp > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lnggp & " gp"
              If Rooms(lngRoomID).Treasure(i).lngPP > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lngPP & " pp"
              
              If Rooms(lngRoomID).Treasure(i).lngGems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objGems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objGems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objGems(j).lngValue & " gp"
                Next j
              End If
              If Rooms(lngRoomID).Treasure(i).lngArt > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objGems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objGems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objGems(j).lngValue & " gp"
                Next j
              End If
              
              If Rooms(lngRoomID).Treasure(i).lngMundaneItems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objItems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objItems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objItems(j).lngValue & " gp"
                Next j
              End If
              If Rooms(lngRoomID).Treasure(i).lngMinorItems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objItems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objItems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objItems(j).lngValue & " gp"
                Next j
              End If
              If Rooms(lngRoomID).Treasure(i).lngMediumItems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objItems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objItems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objItems(j).lngValue & " gp"
                Next j
              End If
              If Rooms(lngRoomID).Treasure(i).lngMajorItems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objItems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objItems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objItems(j).lngValue & " gp"
                Next j
              End If
              If Rooms(lngRoomID).Treasure(i).lngEpicItems > 0 Then
                For j = 1 To Rooms(lngRoomID).Treasure(i).objItems.Count
                  log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).objItems(j).strGemType & " worth " & Rooms(lngRoomID).Treasure(i).objItems(j).lngValue & " gp"
                Next j
              End If
              
              If Rooms(lngRoomID).Treasure(i).lngGems > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lngGems & " gems worth a total of " & CStr(Rooms(lngRoomID).Treasure(i).lngTotalGemValue) & " gp"
              If Rooms(lngRoomID).Treasure(i).lngArt > 0 Then log.writeLogEntry "      " & Rooms(lngRoomID).Treasure(i).lngArt & " pieces of art worth a total of " & CStr(Rooms(lngRoomID).Treasure(i).lngTotalGemValue) & " gp"
              log.writeLogEntry "      " & "Total hoard value = " & CStr(Rooms(lngRoomID).Treasure(i).lngTotalHoardValue) & " gp"
            Next i
          End If
        End If
        
        If Not (Rooms(lngRoomID).MonsterEncounters Is Nothing) Then
          For i = 1 To Rooms(lngRoomID).MonsterEncounters.Count
            With Rooms(lngRoomID).MonsterEncounters(i)
              log.writeLogEntry ""
              log.writeLogEntry "   ENCOUNTER: " & .strName & " (EL " & Format(.sngEL, "##0") & "):"
              If Not (.Data Is Nothing) Then
                For j = 1 To .Data.Count
                  If .Data(j).strMonsterName Like "*Dwarf*" Or _
                    .Data(j).MonsterStatistics(1).strDescriptor Like "*Dwarf*" Then
                    RandomName.strFilename = "dvargar.nam"
                    
                  ElseIf .Data(j).strMonsterName Like "*Elf*" Or _
                    .Data(j).MonsterStatistics(1).strDescriptor Like "*Elf*" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Fey" Then
                    RandomName.strFilename = "alver.nam"
                    
                  ElseIf .Data(j).strMonsterName Like "*Drow*" Then
                    RandomName.strFilename = "drow_names.nam"
                    
                  ElseIf .Data(j).strMonsterName Like "*Dragon*" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Dragon" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Outsider" Then
                    
                    RandomName.strFilename = "mar_draconic.nam"
                    
                  ElseIf .Data(j).strMonsterName Like "*Gnome*" Or _
                    .Data(j).MonsterStatistics(1).strDescriptor Like "*Gnome*" Then
                    RandomName.strFilename = "gnome.nam"
                    
                  ElseIf .Data(j).strMonsterName Like "Halfling*" Or _
                    .Data(j).MonsterStatistics(1).strDescriptor Like "*Halfling*" Then
                    RandomName.strFilename = "rw_english_late_period_names.nam"
                  
                  ElseIf .Data(j).strMonsterName Like "*Human*" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Undead" Or _
                    (.Data(j).MonsterStatistics(1).strDescriptor Like "*Human*" And _
                    Not .Data(j).MonsterStatistics(1).strDescriptor Like "*Humanoid*") Then
                    k = dN(1, 25) - 1
                    RandomName.strFilename = strNameFileList(k)
                    If .Data(j).strMonsterTitle Like "*Gentry*" Then
                      RandomName.strFilename = "Mar_knightly_names.nam"
                    End If
                  
                  ElseIf .Data(j).strMonsterName Like "*Half-Elf*" Or _
                    .Data(j).MonsterStatistics(1).strDescriptor Like "*Half-Elf*" Then
                    k = dN(1, 50) - 1
                    If k < 26 Then
                      RandomName.strFilename = strNameFileList(k)
                    Else
                      RandomName.strFilename = "Alver.nam"
                    End If
                  
                  ElseIf .Data(j).MonsterStatistics(1).strDescriptor Like "*Goblinoid*" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Giant" Or _
                    .Data(j).MonsterStatistics(1).strType Like "Monstrous Humanoid" Then
                    RandomName.strFilename = "orc.nam"
                  
                  ElseIf .Data(j).MonsterStatistics(1).bytIntelligence > 3 Or _
                    .Data(j).MonsterStatistics(1).bytWisdom > 3 Or _
                    .Data(j).MonsterStatistics(1).bytCharisma > 3 Then
                    If .Data(j).MonsterStatistics(1).strType Like "Animal" Then
                      RandomName.strFilename = "alver.nam"
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Magical Beast" Then
                      RandomName.strFilename = "alver.nam"
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Aberration" Then
                      RandomName.strFilename = ""
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Vermin" Then
                      RandomName.strFilename = ""
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Construct" Then
                      RandomName.strFilename = "rw_anglo_saxon_names.nam"
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Humanoid" Then
                      RandomName.strFilename = "orc.nam"
                    ElseIf .Data(j).MonsterStatistics(1).strType Like "Plant" Then
                      RandomName.strFilename = "alver.nam"
                    End If
                  Else
                    RandomName.strFilename = ""
                  End If
                  
                  log.writeLogEntry "      " & .Data(j).strMonsterTitle & " (" & _
                    .Data(j).strMonsterName & ") (" & CStr(.Data(j).lngActualAppearing) & ")"
                  
                  If Not (.Data(j).MonsterStatistics Is Nothing) Then
                    For k = 1 To .Data(j).MonsterStatistics.Count
                      With .Data(j).MonsterStatistics(k)
                        If Len(RandomName.strFilename) > 0 Then
                          If d100 < 51 Then
                            If Not (Right(RandomName.strFilename, 4) Like ".nam") Then
                              RandomName.strFilename = RandomName.strFilename & "Male.nam"
                            End If
                            .strName = RandomName.RandomName("M")
                            log.writeLogEntry "         " & .strName & " (male):"
                          Else
                            If Not (Right(RandomName.strFilename, 4) Like ".nam") Then
                              RandomName.strFilename = RandomName.strFilename & "female.nam"
                            End If
                            .strName = RandomName.RandomName("F")
                            log.writeLogEntry "         " & .strName & " (female):"
                          End If
                        Else
                          If (.strType Like "Aberration" Or .strType Like "Vermin") And _
                            (.bytIntelligence > 3 Or .bytWisdom > 3 Or .bytCharisma > 3) Then
                            
                            .strName = RandomName.WeirdName
                            log.writeLogEntry "         " & .strName & ":"
                          Else
                            .strName = ""
                          End If
                        End If
                        
                        If Not (.Treasure Is Nothing) Then
                          If .Treasure.lngTotalHoardValue > 0 Then
                            log.writeLogEntry "            Treasure:"
                            If .Treasure.lngCP > 0 Then log.writeLogEntry "               " & .Treasure.lngCP & " cp"
                            If .Treasure.lngSP > 0 Then log.writeLogEntry "               " & .Treasure.lngSP & " sp"
                            If .Treasure.lnggp > 0 Then log.writeLogEntry "               " & .Treasure.lnggp & " gp"
                            If .Treasure.lngPP > 0 Then log.writeLogEntry "               " & .Treasure.lngPP & " pp"
                            
                            If .Treasure.lngGems > 0 Then
                              For m = 1 To .Treasure.objGems.Count
                                log.writeLogEntry "               " & .Treasure.objGems(m).strGemType & " worth " & .Treasure.objGems(m).lngValue & " gp"
                              Next m
                            End If
                            If .Treasure.lngArt > 0 Then
                              For m = 1 To .Treasure.objGems.Count
                                log.writeLogEntry "               " & .Treasure.objGems(m).strGemType & " worth " & .Treasure.objGems(m).lngValue & " gp"
                              Next m
                            End If
                            
                            If .Treasure.lngMundaneItems > 0 Then
                              For m = 1 To .Treasure.objItems.Count
                                log.writeLogEntry "               " & .Treasure.objItems(m).strGemType & " worth " & .Treasure.objItems(m).lngValue & " gp"
                              Next m
                            End If
                            If .Treasure.lngMinorItems > 0 Then
                              For m = 1 To .Treasure.objItems.Count
                                log.writeLogEntry "               " & .Treasure.objItems(m).strGemType & " worth " & .Treasure.objItems(m).lngValue & " gp"
                              Next m
                            End If
                            If .Treasure.lngMediumItems > 0 Then
                              For m = 1 To .Treasure.objItems.Count
                                log.writeLogEntry "               " & .Treasure.objItems(m).strGemType & " worth " & .Treasure.objItems(m).lngValue & " gp"
                              Next m
                            End If
                            If .Treasure.lngMajorItems > 0 Then
                              For m = 1 To .Treasure.objItems.Count
                                log.writeLogEntry "               " & .Treasure.objItems(m).strGemType & " worth " & .Treasure.objItems(m).lngValue & " gp"
                              Next m
                            End If
                            If .Treasure.lngEpicItems > 0 Then
                              For m = 1 To .Treasure.objItems.Count
                                log.writeLogEntry "               " & .Treasure.objItems(m).strGemType & " worth " & .Treasure.objItems(m).lngValue & " gp"
                              Next m
                            End If
                            
                            'If .Treasure.lngGems > 0 Then log.writeLogEntry "               " & .Treasure.lngGems & " gems worth a total of " & CStr(.Treasure.lngTotalGemValue) & " gp"
                            'If .Treasure.lngArt > 0 Then log.writeLogEntry "               " & .Treasure.lngArt & " pieces of art worth a total of " & CStr(.Treasure.lngTotalGemValue) & " gp"
                            log.writeLogEntry "               " & "Total hoard value = " & CStr(.Treasure.lngTotalHoardValue) & " gp"
                          End If
                        End If
                        
                      End With
                    Next k
                  End If
                Next j
              End If
            End With
          Next i
        End If
        ProgressBar1.Value = lngRoomID
      Next lngRoomID
    End If
  End If
  Beep
End Sub

