Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module Module1
	
	Sub runme()
		Dim a As Integer
		Dim census() As Integer
		Dim gp() As Integer
		Dim ctr() As Integer
		Dim lvl() As Integer
        Dim iterstart() As Integer
        Dim lifespan() As Integer
        Dim diedfromoldage() As Integer
		Dim i As Integer
		Dim maxlvl As Integer
		Dim ctr1 As Integer
		Dim maxiter As Integer
		Dim el As Integer
		Dim elmod As Integer
        Dim peaklvl As Integer
        Dim ouf As System.IO.StreamWriter

        Randomize(VB.Timer())
        elmod = -4
        maxlvl = 50
        maxiter = 12 * 4 * 5000 ' four encounters per month for 5000 years
        a = 1472655 ' Angmoria adventurer population

        ' export to text
        ouf = New System.IO.StreamWriter("testoutput.txt")
        ouf.Write("ELmod,Encounter,Population")
        ouf.WriteLine()

        'For elmod = -6 To 6
        ReDim census(maxlvl)
        ReDim gp(a)
        ReDim lvl(a)
        ReDim ctr(a)
        ReDim iterstart(a)
        ReDim lifespan(a)
        ReDim diedfromoldage(a)

        census(1) = a

        peaklvl = 1

        For i = 1 To a
            lvl(i) = 1
            iterstart(i) = 0 'Int(Rnd() * maxiter) + 1 ' random introduction to the pool
            lifespan(i) = RollLifeSpan() ' number of adventures before retirement
            diedfromoldage(i) = RollMaxAge(lifespan(i))
        Next


        For ctr1 = 1 To maxiter
            For i = 1 To a

                If lvl(i) > 0 And iterstart(i) <= ctr1 _
                And ctr1 <= iterstart(i) + lifespan(i) Then

                    el = lvl(i) + RollDiff() + elmod
                    If Rnd() <= ChanceToSurvive(el, lvl(i)) Then
                        ' survived, increment ctr
                        'ctr(i) = ctr(i) + 1
                        ctr(i) = ctr(i) + XP(el, lvl(i))

                        gp(i) = gp(i) + CheckPartyCash(lvl(i))

                        If ctr(i) >= XP4NextLevel(lvl(i)) Then
                            'ctr(i) = 0
                            lvl(i) = lvl(i) + 1
                            census(lvl(i) - 1) = census(lvl(i) - 1) - 1
                            census(lvl(i)) = census(lvl(i)) + 1
                            If peaklvl < lvl(i) Then peaklvl = lvl(i)
                        End If
                    Else
                        ' died.  can he be raised by an adept?
                        If gp(i) >= 5000 And lvl(i) > 1 Then
                            census(lvl(i) - 1) = census(lvl(i) - 1) + 1
                            census(lvl(i)) = census(lvl(i)) - 1
                            ctr(i) = XP4NextLevel(lvl(i) - 2)
                            lvl(i) = lvl(i) - 1
                            gp(i) = gp(i) - 5000

                        Else ' dead forever
                            census(lvl(i)) = census(lvl(i)) - 1

                            ' another adventurer comes in to fill footsteps
                            census(1) = census(1) + 1
                            ctr(i) = 0
                            lvl(i) = 1
                            iterstart(i) = ctr1
                            lifespan(i) = RollLifeSpan() ' number of adventures before retirement
                            diedfromoldage(i) = RollMaxAge(lifespan(i))

                            gp(i) = 900
                        End If
                    End If
                    System.Windows.Forms.Application.DoEvents()
                End If

                If diedfromoldage(i) + iterstart(i) < ctr1 Then
                    ' died forever of old age
                    census(lvl(i)) = census(lvl(i)) - 1

                    ' another adventurer comes in to fill footsteps
                    census(1) = census(1) + 1
                    ctr(i) = 0
                    lvl(i) = 1
                    gp(i) = 900
                    iterstart(i) = ctr1
                    lifespan(i) = RollLifeSpan() ' number of adventures before retirement
                    diedfromoldage(i) = RollMaxAge(lifespan(i))
                End If
            Next

            If Form1.Visible = False Then
                Exit For
            End If

            'If ctr1 Mod 4 = 0 Then
            Form1.List1.Items.Clear()
            For i = 1 To peaklvl
                Form1.List1.Items.Add("Level " & CStr(i) & " = " & CStr(census(i)))
            Next i
            ' End If
            Form1.Label1.Text = "Year " & CStr(Int(ctr1 / 50) + 1)

            ouf.Write(CStr(elmod) + "," + CStr(ctr1) + ",")
            For i = 1 To peaklvl
                ouf.Write(CStr(census(i)) + ",")
            Next i
            ouf.WriteLine()

            If ctr1 Mod 100 = 0 Then ' close and re-open the file
                ouf.Close()
                ouf = New System.IO.StreamWriter("testoutput.txt", True)
            End If

            Form1.ProgressBar1.Value = ctr1 * 100 / maxiter
            System.Windows.Forms.Application.DoEvents()


        Next
        'Next

        ouf.Close()
        'Debug.Print("Highest level = " & CStr(peaklvl))
    End Sub
	
	Function CheckPartyCash(ByRef lvl As Integer) As Integer
		Dim partyavailgp As Integer
		Select Case lvl
			Case 1
				partyavailgp = 900 / 14
			Case 2
				partyavailgp = (2000 - 900) / 14
			Case 3
				partyavailgp = (2500 - 2000) / 14
			Case 4
				partyavailgp = (3300 - 2500) / 14
			Case 5
				partyavailgp = (4300 - 3300) / 14
			Case 6
				partyavailgp = (4600 - 4300) / 14
			Case 7
				partyavailgp = (7200 - 4600) / 14
			Case 8
				partyavailgp = (9400 - 7200) / 14
			Case 9
				partyavailgp = (12000 - 9400) / 14
			Case 10
				partyavailgp = (16000 - 12000) / 14
			Case 11
				partyavailgp = (21000 - 16000) / 14
			Case 12
				partyavailgp = (27000 - 21000) / 14
			Case 13
				partyavailgp = (35000 - 27000) / 14
			Case 14
				partyavailgp = (45000 - 35000) / 14
			Case 15
				partyavailgp = (59000 - 45000) / 14
			Case 16
				partyavailgp = (77000 - 59000) / 14
			Case 17
				partyavailgp = (100000 - 77000) / 14
			Case 18
				partyavailgp = (130000 - 100000) / 14
			Case 19
				partyavailgp = (220000 - 130000) / 14
			Case 20
				partyavailgp = (270000 - 220000) / 14
			Case 21
				partyavailgp = (330000 - 270000) / 14
		End Select
		CheckPartyCash = partyavailgp
	End Function
	
	Function XP(ByRef cr As Integer, ByRef lvl As Integer) As Integer
        Select Case cr - lvl
            Case Is < -8
                XP = 0
            Case -8
                XP = lvl * 18.75 / 4
            Case -7
                XP = lvl * 25 / 4
            Case -6
                XP = lvl * 37.5 / 4
            Case -5
                XP = lvl * 50 / 4
            Case -4
                XP = lvl * 75 / 4
            Case -3
                XP = lvl * 100 / 4
            Case -2
                XP = lvl * 150 / 4
            Case -1
                XP = lvl * 200 / 4
            Case 0
                XP = lvl * 300 / 4
            Case 1
                XP = lvl * 400 / 4
            Case 2
                XP = lvl * 600 / 4
            Case 3
                XP = lvl * 800 / 4
            Case 4
                XP = lvl * 1200 / 4
            Case 5
                XP = lvl * 1600 / 4
            Case 6
                XP = lvl * 2400 / 4
            Case 7
                XP = lvl * 3200 / 4
            Case 8
                XP = lvl * 4800 / 4
            Case Is > 8
                If (cr - lvl) Mod 2 = 0 Then
                    'even level
                    XP = lvl * 4800 * 2 ^ ((cr - lvl - 8 + 1) / 2) / 4
                Else
                    XP = lvl * 3200 * 2 ^ ((cr - lvl - 7 + 1) / 2) / 4
                End If

        End Select

	End Function
	
	Function XP4NextLevel(ByRef lvl As Integer) As Integer
		XP4NextLevel = 500 * lvl * (lvl + 1)
	End Function
	
	
	
	Function ChanceToSurvive(ByRef el As Integer, ByRef lvl As Integer) As Single
		Select Case el
            Case Is < lvl - 8
                ChanceToSurvive = 1
            Case lvl - 8
                ChanceToSurvive = 0.998
			Case lvl - 7
				ChanceToSurvive = 0.99
			Case lvl - 6
				ChanceToSurvive = 0.984
			Case lvl - 5
				ChanceToSurvive = 0.98
			Case lvl - 4
				ChanceToSurvive = 0.968
			Case lvl - 3
				ChanceToSurvive = 0.9575
			Case lvl - 2
				ChanceToSurvive = 0.937
			Case lvl - 1
				ChanceToSurvive = 0.915
			Case lvl
				ChanceToSurvive = 0.875
			Case lvl + 1
				ChanceToSurvive = 0.825
			Case lvl + 2
				ChanceToSurvive = 0.75
			Case lvl + 3
				ChanceToSurvive = 0.63
			Case lvl + 4
				ChanceToSurvive = 0.5
			Case lvl + 5
                ChanceToSurvive = 0.375
            Case lvl + 6
                ChanceToSurvive = 0.25
            Case lvl + 7
                ChanceToSurvive = 0.18333
            Case lvl + 8
                ChanceToSurvive = 0.125
            Case Is > lvl + 8
                ChanceToSurvive = 0.0625
        End Select
	End Function
	
	Function RollDiff() As Integer
		Select Case Int(Rnd() * 100) + 1
			Case Is < 51
				RollDiff = Int(Rnd() * 5) - 5 ' -5 to -1
			Case 51 To 75
				RollDiff = Int(Rnd() * 3) - 1 ' -1 to +1
			Case 76 To 85
				RollDiff = Int(Rnd() * 2) + 2 ' +2 to +3
			Case 86 To 95
				RollDiff = Int(Rnd() * 2) + 3 ' +3 to +4
			Case Else
				RollDiff = Int(Rnd() * 2) + 4 ' +4 to +5
		End Select
    End Function

    Function RollLifeSpan() As Integer
        Select Case Int(Rnd() * 100) + 1
            Case Is <= 37 ' human
                RollLifeSpan = 2432
            Case 38 To 57 ' halfling
                RollLifeSpan = 3368
            Case 58 To 75 ' elf
                RollLifeSpan = 10400
            Case 76 To 85 ' dwarf
                RollLifeSpan = 9240
            Case 86 To 93 ' gnome
                RollLifeSpan = 6608
            Case 94 To 98 ' half-elf
                RollLifeSpan = 4704
            Case Else ' half-orc
                RollLifeSpan = 2000

        End Select
    End Function

    Function RollMaxAge(ByVal lspan As Integer) As Integer
        Select Case lspan
            Case 2432 ' human
                RollMaxAge = 12 * 4 * (70 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1)
            Case 9240 ' dwarf
                RollMaxAge = 12 * 4 * (250 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1)
            Case 10400 ' dwarf
                RollMaxAge = 12 * 4 * (350 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1)
            Case 6608 ' gnome
                RollMaxAge = 12 * 4 * (200 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1 + Int(Rnd() * 100) + 1)
            Case 4704 ' half-elf
                RollMaxAge = 12 * 4 * (125 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1)
            Case 2000 ' hlaf-orc
                RollMaxAge = 12 * 4 * (60 + Int(Rnd() * 10) + 1 + Int(Rnd() * 10) + 1)
            Case 3368 ' halfling
                RollMaxAge = 12 * 4 * (100 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1 + Int(Rnd() * 20) + 1)
        End Select
    End Function
End Module