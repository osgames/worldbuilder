Option Strict Off
Option Explicit On
Friend Class CCollageFractal
	
	' Creates random heightmaps by the collage method or the fractal method
	
    Private mvarZ(,) As Short
	
	Private mvarZCoast As Integer
	Private mvarZShelf As Integer
	'Private mvarCComposition As CComposition
	
	
	'Public Property Get CComposition() As CComposition
	'    If mvarCComposition Is Nothing Then
	'        Set mvarCComposition = New CComposition
	'    End If
	'
	'
	'    Set CComposition = mvarCComposition
	'End Property
	
	
	'Public Property Set CComposition(vData As CComposition)
	'    Set mvarCComposition = vData
	'End Property
	'Private Sub Class_Terminate()
	'    Set mvarCComposition = Nothing
	'End Sub
	
	
	
	
	Public Property ZCoast() As Integer
		Get
			ZCoast = mvarZCoast
		End Get
		Set(ByVal Value As Integer)
			mvarZCoast = Value
		End Set
	End Property
	
	Public Property ZShelf() As Integer
		Get
			ZShelf = mvarZShelf
		End Get
		Set(ByVal Value As Integer)
			mvarZShelf = Value
		End Set
	End Property
	
	
	
    Public Function GetHiResDEM() As Short(,)
        GetHiResDEM = VB6.CopyArray(mvarZ)
    End Function
	
	
	Public Function CollageTerrain(ByRef RDimIn As Integer, ByRef CDimIn As Integer, ByRef ZDelta As Integer, ByRef MaxR As Integer, ByRef MaxAlt As Integer, ByRef ZMin As Integer, Optional ByRef UseExistingZ As Boolean = False) As Short()
		
		Dim i As Integer
        Dim Z(,) As Short
		Dim RDim, CDim As Integer
		Dim radC, newC, newR, radR, altZ As Integer
		Dim ro, co As Integer
        Dim newZ(,) As Integer
		Dim r, c As Integer
		
		RDim = RDimIn
		CDim = CDimIn
		
		If UseExistingZ = True Then
			Z = VB6.CopyArray(mvarZ)
		Else
			ReDim Z(RDim, CDim)
		End If
		
		i = 0
		Do While i < ZDelta 'MaxMax(Z) - MinMin(Z) < ZDelta
			i = i + 1
			
			newR = ceil(randn * RDim / 2 + RDim / 2)
			newC = ceil(randn * CDim / 2 + CDim / 2)
			radR = ceil(Rnd() * MaxR)
			radC = ceil(Rnd() * MaxR)
			altZ = randn * MaxAlt
			
			ReDim newZ(RDim, CDim)
			
			For ro = (newR - radR) To (newR + radR)
				For co = (newC - radC) To (newC + radC)
					If ro >= 0 And ro <= RDim And co >= 0 And co <= CDim Then
						
						r = ro - newR
						c = co - newC
						
						newZ(ro, co) = radR / MaxR * (1 - (r ^ 2 / radR ^ 2) - (c ^ 2 / radC ^ 2))
						If newZ(ro, co) < 0 Then newZ(ro, co) = 0
						If Z(ro, co) + altZ * newZ(ro, co) + ZMin > 32767 Then
							Z(ro, co) = 32767
						Else
							Z(ro, co) = Z(ro, co) + altZ * newZ(ro, co) + ZMin
						End If
						If Z(ro, co) < 0 Then Z(ro, co) = 0
						
					End If
				Next co
			Next ro
			System.Windows.Forms.Application.DoEvents()
		Loop 
		
		mvarZ = VB6.CopyArray(Z)
		CollageTerrain = VB6.CopyArray(Z)
	End Function
	
	'UPGRADE_NOTE: MY was upgraded to MY_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function getdata(ByRef X As Integer, ByRef Y As Integer, ByRef MX As Integer, ByRef MY_Renamed As Integer) As Single
		' Get an array element
		Dim L As Single
		
		If Y > MY_Renamed Then
			L = mvarZ(MX + 1 - Y + 1, MX - X + 1)
		Else
			L = mvarZ(Y + 1, X + 1)
		End If
		getdata = L
	End Function
	
	
    Public Sub FractalTerrainInterp(ByRef BaseArray(,) As Short, ByRef Levels As Integer, Optional ByRef NoWrap As Boolean = False)

        'Dim OutputArray() As Long

        Dim i, j As Integer
        Dim n As Integer

        Dim ratio As Integer
        Dim mountainscale As Single
        Dim baseRows As Integer
        Dim baseCols As Integer
        Dim newRows, newCols As Integer
        Dim stepsize As Integer
        Dim altdev As Integer
        Dim tempZ As Integer
        Dim sealevel As Double
        Dim thisRowHi As Integer
        Dim thisColHi As Integer
        Dim thisRowLo As Integer
        Dim thisColLo As Integer

        Dim maxZ, minZ As Integer

        baseRows = UBound(BaseArray, 1)
        baseCols = UBound(BaseArray, 2)

        ratio = 2 ^ Levels

        newRows = baseRows * 2 ^ Levels
        newCols = baseCols * 2 ^ Levels

        ReDim mvarZ(newRows, newCols)

        mountainscale = MAXMOUNTAINHEIGHT * 100.0# / 255
        altdev = mountainscale
        sealevel = mvarZCoast * mountainscale

        maxZ = 0
        minZ = 1000000.0#

        ' Copy the base array into the new array, and scale it into feet
        For i = 0 To baseRows
            For j = 0 To baseCols
                If CInt(BaseArray(i, j)) * CInt(altdev) <= 32767 Then
                    mvarZ(i * ratio, j * ratio) = CInt(BaseArray(i, j)) * CInt(altdev)
                    If CInt(BaseArray(i, j)) * CInt(altdev) < minZ Then minZ = CInt(BaseArray(i, j)) * CInt(altdev)
                    If CInt(BaseArray(i, j)) * CInt(altdev) > maxZ Then maxZ = CInt(BaseArray(i, j)) * CInt(altdev)
                Else
                    mvarZ(i * ratio, j * ratio) = 32767
                    If 32767 < minZ Then minZ = 32767
                    If 32767 > maxZ Then maxZ = 32767
                End If
            Next j
        Next i

        ' setup the basics
        stepsize = ratio

        frmTec.ProgressBar1.Value = 0
        If Levels > 0 Then
            frmTec.ProgressBar1.Maximum = Levels
        Else
            frmTec.ProgressBar1.Maximum = 1
        End If

        For n = 1 To Levels
            ' cut the altitude deviation in half
            'altdev = altdev / 2

            ' interpolate main rows
            For i = 0 To newRows Step stepsize
                For j = stepsize / 2 To newCols Step stepsize
                    thisColLo = j - stepsize / 2
                    thisColHi = j + stepsize / 2

                    If thisColLo >= 0 And thisColHi <= newCols Then

                        mvarZ(i, j) = Int((CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, thisColHi))) / 2)

                    ElseIf thisColLo < 0 Then  ' wrap around

                        If NoWrap = False Then
                            mvarZ(i, j) = Int((CInt(mvarZ(i, newCols)) + CInt(mvarZ(i, thisColHi))) / 2)
                        Else
                            mvarZ(i, j) = mvarZ(i, thisColHi)
                        End If

                    Else ' wrap around the other way

                        If NoWrap = False Then
                            mvarZ(i, j) = Int((CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, 0))) / 2)
                        Else
                            mvarZ(i, j) = mvarZ(i, thisColLo)
                        End If
                    End If

                    ' introduce a random element
                    If mvarZ(i, j) >= sealevel Then
                        tempZ = mvarZ(i, j) + randn() * altdev
                        If tempZ > 32767 Then tempZ = 32767
                        mvarZ(i, j) = tempZ
                    End If
                    If mvarZ(i, j) < 0 Then
                        mvarZ(i, j) = 0
                    End If
                    'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#

                    If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
                    If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
                Next j
            Next i

            ' interpolate main cols
            For i = stepsize / 2 To newRows Step stepsize
                thisRowLo = i - stepsize / 2
                thisRowHi = i + stepsize / 2

                For j = 0 To newCols Step stepsize

                    If thisRowLo >= 0 And thisRowHi <= newRows Then
                        mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowHi, j))) / 2)
                    ElseIf thisRowLo < 0 Then
                        mvarZ(i, j) = mvarZ(0, j)
                    ElseIf thisRowHi > newRows Then
                        mvarZ(i, j) = mvarZ(newRows, j)
                    End If

                    ' introduce a random element
                    If mvarZ(i, j) >= sealevel Then
                        tempZ = mvarZ(i, j) + randn() * altdev
                        If tempZ > 32767 Then tempZ = 32767
                        mvarZ(i, j) = tempZ
                    End If
                    If mvarZ(i, j) < 0 Then
                        mvarZ(i, j) = 0
                    End If
                    'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
                    If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
                    If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
                Next j
            Next i

            ' interpolate diagonally
            For i = stepsize / 2 To newRows Step stepsize
                thisRowLo = i - stepsize / 2
                thisRowHi = i + stepsize / 2

                If thisRowLo >= 0 And thisRowHi <= newRows Then

                    For j = stepsize / 2 To newCols Step stepsize
                        thisColLo = j - stepsize / 2
                        thisColHi = j + stepsize / 2

                        If thisColLo >= 0 And thisColHi <= newCols Then
                            ' eight-way
                            mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, thisColLo)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, thisColLo)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 8)
                        ElseIf thisColLo < 0 Then
                            If NoWrap = False Then
                                ' eight-way across left edge
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, newCols)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, newCols)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, newCols)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 8)
                            Else
                                ' five-way across left edge
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 5)
                            End If
                        Else
                            If NoWrap = False Then
                                ' eight-way across right edge
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, 0)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, 0)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 8)
                            Else
                                ' five-way across right edge
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 5)
                            End If
                        End If

                        ' introduce a random element
                        If mvarZ(i, j) >= sealevel Then
                            tempZ = mvarZ(i, j) + randn() * altdev
                            If tempZ > 32767 Then tempZ = 32767
                            mvarZ(i, j) = tempZ
                        End If

                        If mvarZ(i, j) < 0 Then
                            mvarZ(i, j) = 0
                        End If
                        'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
                        If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
                        If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
                    Next j

                ElseIf thisRowLo < 0 Then
                    For j = stepsize / 2 To newCols Step stepsize
                        thisColLo = j - stepsize / 2
                        thisColHi = j + stepsize / 2
                        If thisColLo >= 0 And thisColHi <= newCols Then
                            ' five-way across top edge
                            mvarZ(i, j) = Int((CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, thisColLo)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 5)
                        ElseIf thisColLo < 0 Then
                            If NoWrap = False Then
                                'five-way across top left corner
                                mvarZ(i, j) = Int((CInt(mvarZ(i, newCols)) + CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, newCols)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 5)
                            Else
                                'five-way across top left corner
                                mvarZ(i, j) = Int((CInt(mvarZ(i, thisColHi)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, thisColHi))) / 3)
                            End If
                        Else
                            If NoWrap = False Then
                                ' five-way across top right corner
                                mvarZ(i, j) = Int((CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, 0)) + CInt(mvarZ(thisRowHi, thisColLo)) + CInt(mvarZ(thisRowHi, j)) + CInt(mvarZ(thisRowHi, 0))) / 5)
                            Else
                                ' five-way across top right corner
                                mvarZ(i, j) = Int((CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(thisRowHi, thisColLo)) + CInt(mvarZ(thisRowHi, j))) / 3)
                            End If
                        End If

                        ' introduce a random element
                        If mvarZ(i, j) >= sealevel Then
                            tempZ = mvarZ(i, j) + randn() * altdev
                            If tempZ > 32767 Then tempZ = 32767
                            mvarZ(i, j) = tempZ
                        End If


                        If mvarZ(i, j) < 0 Then
                            mvarZ(i, j) = 0
                        End If
                        'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
                        If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
                        If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
                    Next j

                Else
                    For j = stepsize / 2 To newCols Step stepsize
                        thisColLo = j - stepsize / 2
                        thisColHi = j + stepsize / 2
                        If thisColLo >= 0 And thisColHi <= newCols Then
                            ' five-way across bottom edge
                            mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, thisColLo)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, thisColHi))) / 5)
                        ElseIf thisColLo < 0 Then
                            If NoWrap = False Then
                                ' five-way across bottem left corner
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, newCols)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, newCols)) + CInt(mvarZ(i, thisColHi))) / 5)
                            Else
                                ' five-way across bottem left corner
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, thisColHi)) + CInt(mvarZ(i, thisColHi))) / 3)
                            End If
                        Else
                            If NoWrap = False Then
                                'five-way across bottom right corner
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, thisColLo)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(thisRowLo, 0)) + CInt(mvarZ(i, thisColLo)) + CInt(mvarZ(i, 0))) / 5)
                            Else
                                'five-way across bottom right corner
                                mvarZ(i, j) = Int((CInt(mvarZ(thisRowLo, thisColLo)) + CInt(mvarZ(thisRowLo, j)) + CInt(mvarZ(i, thisColLo))) / 3)
                            End If
                        End If


                        ' introduce a random element
                        If mvarZ(i, j) >= sealevel Then
                            tempZ = mvarZ(i, j) + randn() * altdev
                            If tempZ > 32767 Then tempZ = 32767
                            mvarZ(i, j) = tempZ
                        End If
                        If mvarZ(i, j) < 0 Then
                            mvarZ(i, j) = 0
                        End If
                        'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
                        If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
                        If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
                    Next j
                End If

            Next i



            stepsize = stepsize / 2 ' do it at the end because we need to skip the points we've already got

            frmTec.ProgressBar1.Value = n
            System.Windows.Forms.Application.DoEvents()
        Next n


        'frmTec.StatusBar1.Items.Item(5).Text = "Max elevation = " & VB6.Format(maxZ - (mvarZCoast * altdev), "##,##0") & " ft"


    End Sub
	
	Public Sub DrawHiRes()
		
		Dim i As Integer
		Dim j As Integer
		Dim MaxAlt As Integer
		For i = 0 To UBound(mvarZ, 1)
			For j = 0 To UBound(mvarZ, 2)
				If mvarZ(i, j) > MaxAlt Then MaxAlt = mvarZ(i, j)
			Next j
		Next i
		
        frmTec.StatusBar1.Items.Item(5).Text = "Max elevation = " & VB6.Format(MaxAlt, "##,##0") & " ft"
		'frmTec.lblMinVal.Caption = "0 ft"
		'frmTec.lblMaxVal.Caption = "34,000 ft"
		
        'draw_(modTecGlobals.DrawType_E.DRAW_TEC, mvarZ)
	End Sub
	
	
	'Private Sub assign_colors()
	'  Dim i As Long
	'
	'  FillX11ColorTable
	'
	'  color_values(0) = &H0 'black
	'  color_values(1) = &HFFFFFF 'white
	'  color_values(2) = &HBFBFBF 'grey75
	'  color_values(3) = &H7F7F7F 'grey50
	'  color_values(4) = &HAB82FF 'MediumPurple1
	'  color_values(5) = &H551A8B 'purple4
	'  color_values(6) = &H7D26CD 'purple3
	'  color_values(7) = &H912CEE 'purple2
	'  color_values(8) = &H9B30FF 'purple1
	'  color_values(9) = RGB(0, 0, &H8B) 'blue4
	'  color_values(10) = RGB(0, 0, &HCD) 'blue3
	'  color_values(11) = RGB(0, 0, &HEE) 'blue2
	'  color_values(12) = RGB(0, 0, &HFF)     'blue1
	'  color_values(13) = RGB(0, &H64, 0) 'DarkGreen
	'  color_values(14) = RGB(0, &H8B, 0) 'green4
	'  color_values(15) = RGB(0, &HCD, 0) 'green3
	'  color_values(16) = RGB(0, &HEE, 0) 'green2
	'  color_values(17) = RGB(0, &HFF, 0) 'green1
	'  color_values(18) = &H8B6508 'DarkGoldenrod4
	'  color_values(19) = &H8B8B00 'yellow4
	'  color_values(20) = &HCDCD00 'yellow3
	'  color_values(21) = &HEEEE00 'yellow2
	'  color_values(22) = &HFFFF00 'yellow1
	'  color_values(23) = &H8B5A00 'orange4
	'  color_values(24) = &HCD8500 'orange3
	'  color_values(25) = &HEE9A00 'orange2
	'  color_values(26) = &HFFA500 'orange1
	'  color_values(27) = &H8B2323 'brown4
	'  color_values(28) = &H8B0000 'red4
	'  color_values(29) = &HCD0000 'red3
	'  color_values(30) = &HEE0000 'red2
	'  color_values(31) = &HFF0000 'red1
	'
	'  climcols(0) = color_values(9)
	'  climcols(1) = color_values(20)
	'  climcols(2) = color_values(18)
	'  climcols(3) = color_values(2)
	'  climcols(4) = color_values(3)
	'  climcols(5) = color_values(18)
	'  climcols(6) = color_values(22)
	'  climcols(7) = color_values(26)
	'  climcols(8) = color_values(14)
	'  climcols(9) = color_values(17)
	'  climcols(10) = color_values(12)
	'
	'  landcols(0) = color_values(11)
	'  landcols(1) = color_values(28)
	'  landcols(2) = color_values(3)
	'
	'
	'  For i = 0 To 255
	'    'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
	'    greycols(i) = RGB(i, i, i)
	'  Next i
	'
	'  For i = 1 To MAXPLATE
	'    platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
	'  Next i
	'
	'  ReDim tecols(MAXMOUNTAINHEIGHT) As Long
	'  ' deep ocean
	'  For i = 0 To mvarZShelf
	'    tecols(i) = RGB(0, 0, 139) ' dark blue
	'  Next i
	'
	'  ' continental shelf
	'  For i = mvarZShelf To mvarZCoast
	'    tecols(i) = RGB(128, 255, 255) ' light blue
	'  Next i
	'
	'  ' Greens for land
	'  For i = mvarZCoast To mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255)
	'    'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
	'    If i >= UBound(tecols) Then Exit For
	'    tecols(i) = RGB(0, i * 2 + 95, 0)
	'  Next i
	'
	'  ' Brown for above tree line
	'  For i = mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255)
	'    If i >= UBound(tecols) Then Exit For
	'    tecols(i) = RGB(128, i, i)
	'  Next i
	'
	'  ' White for above snow line
	'  For i = mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
	'    If i >= UBound(tecols) Then Exit For
	'    'tecols(i) = color_values(1)
	'    tecols(i) = RGB(150 + i / 2, 150 + i / 2, 150 + i / 2)
	'  Next i
	'
	'  For i = mvarZCoast + 200 To UBound(tecols)
	'    If i >= UBound(tecols) Then Exit For
	'    'tecols(i) = color_values(1)
	'    tecols(i) = RGB(255, 255, 255)
	'  Next i
	'
	'
	'
	'End Sub
	
	
	
	
	
	
    Private Sub Class_Initialize_Renamed()
        mvarZShelf = 8
        mvarZCoast = 16
    End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class