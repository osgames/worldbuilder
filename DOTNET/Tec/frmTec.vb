Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Drawing
Friend Class frmTec
	Inherits System.Windows.Forms.Form
	
	
	Public tec As CTec
	Public frac As CCollageFractal
	Dim hiresdata() As Integer
	Dim t() As Integer
	
    'Private Sub cboDraw_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
    '    'Dim popden() As Single
    '    'Dim i, j As Integer

    '    Select Case cboDraw.Text
    '        Case "Plates"
    '            tec.platedraw()
    '        Case "Topography"
    '            tec.DrawTopographicReliefMap()
    '    End Select
    '    'DrawGrid()
    '    'DrawRegionGrid()
    'End Sub
	
    'Private Sub cboSeason_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
    '    cboDraw_SelectedIndexChanged(cboDraw, New System.EventArgs())
    'End Sub
	
	
	
    'Private Sub cmdShowParameters_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowParameters.Click
    '	VB6.ShowForm(frmTecParameters, VB6.FormShowConstants.Modal, Me)
    'End Sub
	
	Private Sub Command1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Command1.Click
        Dim i As Integer, j As Integer
		Dim dtmStart As Date
		Dim dtmEnd As Date

        SetParams()

        Command1.Enabled = False
        cboSize.Enabled = False
		
		MAXX = 1024
		MAXY = 1024
		
		
		dtmStart = Now
		
		tec.XSize = 512
		tec.YSize = 256

		
		n = ((tec.XSize / 2) - 1)
		area = (tec.XSize * tec.XSize)

        System.Windows.Forms.Application.DoEvents()
		
        tec.MaxStep = CInt(txtMaxSteps.Text)
		With tec
			.DrawEvery = 1
			For i = 0 To MAXPLATE
				.Plates.Add()
			Next i
			
			.RunModel((tec.MaxStep))
		End With

        lblStep.Text = "Interpolating image"
        Application.DoEvents()

		' interpolate the 512x256 into an 8192x4096, with random bumping
		frac.FractalTerrainInterp(tec.GetHeightMap, (cboSize.SelectedIndex))

        Dim HiResDEM(,) As Short
        Dim newHeight As Short
        HiResDEM = frac.GetHiResDEM()
        Dim outputbumpmap As New System.Drawing.Bitmap(HiResDEM.GetUpperBound(0), HiResDEM.GetUpperBound(1))

        Dim lut(255) As Color
        For i = 0 To 255
            lut(i) = Color.FromArgb(i, i, i)
        Next

        lblStep.Text = "Saving image"
        ProgressBar1.Minimum = 0
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = outputbumpmap.Height * outputbumpmap.Width
        For i = 0 To outputbumpmap.Height - 1
            For j = 0 To outputbumpmap.Width - 1
                newHeight = HiResDEM(j, i) * 255 / 32767
                outputbumpmap.SetPixel(j, i, lut(newHeight))
                ProgressBar1.Value = ProgressBar1.Value + 1
                Application.DoEvents()
            Next
        Next
        outputbumpmap.Save("bumpmap.bmp", System.Drawing.Imaging.ImageFormat.Bmp)

        PictureBox1.ImageLocation = "bumpmap.bmp"
        PictureBox1.Load()

        lblStep.Text = "Saving elevation difference"
        outputbumpmap = New System.Drawing.Bitmap(HiResDEM.GetUpperBound(0), HiResDEM.GetUpperBound(1))
        ProgressBar1.Minimum = 0
        ProgressBar1.Value = 0
        ProgressBar1.Maximum = outputbumpmap.Height * outputbumpmap.Width
        For i = 0 To outputbumpmap.Height - 2
            For j = 0 To outputbumpmap.Width - 1

                If j = outputbumpmap.Width - 1 Then
                    newHeight = HiResDEM(0, i + 1) - HiResDEM(j, i)
                Else
                    newHeight = HiResDEM(j + 1, i + 1) - HiResDEM(j, i)
                End If

                newHeight = newHeight / 256 + 128
                outputbumpmap.SetPixel(j, i, lut(newHeight))
                ProgressBar1.Value = ProgressBar1.Value + 1
                Application.DoEvents()
            Next
        Next
        outputbumpmap.Save("lightmap.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
        PictureBox1.ImageLocation = "lightmap.bmp"
        PictureBox1.Load()


        dtmEnd = Now

        StatusBar1.Items.Item(0).Text = "Time to complete = " & VB6.Format(System.DateTime.FromOADate(dtmEnd.ToOADate - dtmStart.ToOADate), "hh:mm:ss")

        'cboDraw.Enabled = True
        'If cboDraw.SelectedIndex <> 1 Then
        '    cboDraw.SelectedIndex = 1 'cboDraw.ListCount - 1
        'Else
        '    cboDraw_SelectedIndexChanged(cboDraw, New System.EventArgs())
        'End If

        cboSize.Enabled = True
        Command1.Enabled = True
        'cmdShowParameters.Enabled = True
    End Sub
	
	Private Sub frmTec_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'Dim i As Integer
        'Const MAXPLATE As Short = 40
		
		MAXMOUNTAINHEIGHT = 327
		Randomize(VB.Timer())
		TRandomInit(VB.Timer())
		
		Me.Show()
		cboSize.SelectedIndex = 0
		
        'XWinScale = VB6.PixelsToTwipsX(Me.Picture1.ClientRectangle.Width)
        'YWinScale = VB6.PixelsToTwipsY(Me.Picture1.ClientRectangle.Height)
		
		tec = New CTec
		frac = New CCollageFractal
		System.Windows.Forms.Application.DoEvents()
		
		tec.BendEvery = 6
		tec.HydroPct = 75
		tec.MaxStep = 25
		
        'VB6.ShowForm(frmTecParameters, VB6.FormShowConstants.Modal, Me)
        With tec
            txtBendBy.Text = CStr(.BendBy)
            txtBendEvery.Text = CStr(.BendEvery)
            txtBumpTol.Text = CStr(.BumpTol)
            If .DoErode = 1 Then
                chkDoErode.CheckState = System.Windows.Forms.CheckState.Checked
            Else
                chkDoErode.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If
            txtErodeRnd.Text = CStr(.ErodeRnd)
            .DrawEvery = 1
            .ExportBitmap = False
            txtHydroPct.Text = CStr(.HydroPct)
            txtMaxBump.Text = CStr(.MaxBump)
            txtMaxCtrTry.Text = CStr(.MaxCtrTry)
            txtMaxSteps.Text = CStr(.MaxStep)
            txtRiftDist.Text = CStr(.RiftDist)
            txtRiftPct.Text = CStr(.RiftPct)
            txtSpeedBase.Text = CStr(.SpeedBase)
            txtSpeedRng.Text = CStr(.SpeedRng)
            txtZCoast.Text = CStr(.ZCoast)
            txtZErode.Text = CStr(.ZErode)
            txtZInit.Text = CStr(.ZInit)
            txtZShelf.Text = CStr(.ZShelf)
            txtZSubsume.Text = CStr(.ZSubsume)
        End With
	End Sub
	
    Private Sub SetParams()
        tec = New CTec
        'Set frmTec.clim = New CClim
        frac = New CCollageFractal

        With tec
            .BendBy = CInt(txtBendBy.Text)
            .BendEvery = CInt(txtBendEvery.Text)
            .BumpTol = CInt(txtBumpTol.Text)
            If chkDoErode.CheckState = System.Windows.Forms.CheckState.Checked Then
                .DoErode = 1
            Else
                .DoErode = 0
            End If
            .DrawEvery = 1
            .ErodeRnd = CInt(txtErodeRnd.Text)
            .ExportBitmap = False
            .HydroPct = CInt(txtHydroPct.Text)
            .MaxBump = CInt(txtMaxBump.Text)
            .MaxCtrTry = CInt(txtMaxCtrTry.Text)
            .MaxStep = CInt(txtMaxSteps.Text)
            .RiftDist = CInt(txtRiftDist.Text)
            .RiftPct = CInt(txtRiftPct.Text)
            .SpeedBase = CInt(txtSpeedBase.Text)
            .SpeedRng = CInt(txtSpeedRng.Text)
            .ZCoast = CInt(txtZCoast.Text)
            .ZErode = CInt(txtZErode.Text)
            .ZInit = CInt(txtZInit.Text)
            .ZShelf = CInt(txtZShelf.Text)
            .ZSubsume = CInt(txtZSubsume.Text)
        End With

        With frac
            .ZCoast = tec.ZCoast
            .ZShelf = tec.ZShelf
        End With
    End Sub
	
    Private Function MakeSmallDEM(ByRef MainDEM(,) As Integer) As Integer()
        Dim SmallDem(,) As Integer
        Dim i, j As Integer
        Dim ii, jj As Integer
        ReDim SmallDem(128, 64)

        For i = 0 To 127
            For j = 0 To 63
                SmallDem(i, j) = 0
                For ii = 0 To 3
                    For jj = 0 To 3
                        SmallDem(i, j) = SmallDem(i, j) + MainDEM(i * 4 + ii, j * 4 + jj)
                    Next jj
                Next ii
                SmallDem(i, j) = SmallDem(i, j) / 16
            Next j
        Next i
        MakeSmallDEM = VB6.CopyArray(SmallDem)
    End Function
	
    'Public Function DrawColorBar(ByRef lut() As Integer) As Object
    '	Dim i As Integer
    '	Dim stepwidth As Integer
    '	stepwidth = VB6.PixelsToTwipsX(Picture2.ClientRectangle.Width) / UBound(lut)

    '	For i = 0 To UBound(lut)
    '		'UPGRADE_ISSUE: PictureBox method Picture2.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '		Picture2.Line (i * stepwidth, 0) - (stepwidth, VB6.PixelsToTwipsY(Picture2.ClientRectangle.Height)), lut(i), BF
    '	Next i

    'End Function
	
	Private Sub frmTec_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		'UPGRADE_NOTE: Object tec may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		tec = Nothing
		'UPGRADE_NOTE: Object frac may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		frac = Nothing
    End Sub


    
End Class