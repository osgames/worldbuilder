Option Strict Off
Option Explicit On


Imports Microsoft.VisualBasic.Compatibility

Public Class CClim



    Private Const mvarSmallXSize As Short = 128
    Private Const mvarSmallYSize As Short = 64

    Private mvarZCoast As Integer
    Private mvarZShelf As Integer



    Private MaxAlt As Integer
    Private mintemp As Integer ' F
    Private maxtemp As Integer ' F

    Private MinRain As Integer
    Private MaxRain As Integer
    Private MvarRainScale As Double

    Private Const TREELINE As Short = 37
    Private Const SNOWLINE As Short = 133
    Private mvarXSize As Integer
    Private mvarYSize As Integer
    Private mvarBSize As Integer

    Private mvarTilt As Double
    Private mvarEccentricity As Double
    Private mvarEccPhase As Double
    Private mvarLandTemperatureMean As Double
    Private mvarLandTemperatureRange As Double
    Private mvarOceanTemperatureMean As Double
    Private mvarOceanTemperatureRange As Double

    Private mvarOLThresh As Integer
    Private mvarOOThresh As Integer
    Private mvarOLMin As Integer
    Private mvarOHMax As Integer
    Private mvarOLMax As Integer
    Private mvarOHMin As Integer
    Private mvarLOThresh As Integer
    Private mvarLLThresh As Integer
    Private mvarLLMin As Integer
    Private mvarLLMax As Integer
    Private mvarLHMin As Integer
    Private mvarLHMax As Integer
    Private mvarBarSep As Integer
    Private mvarMaxFetch As Integer
    Private mvarRainConst As Integer
    Private mvarLandDelta As Single
    Private mvarMountainDelta As Integer
    Private mvarFetchDelta As Integer
    Private mvarHeatEquatorDelta As Integer
    Private mvarNearHeatEquatorDelta As Integer ' Near heat equator delta
    Private mvarFlankDelta As Integer
    Private mvarNearFlankDelta As Integer
    Private mvarIcebergK As Integer
    Private mvarMaxRange As Integer

    '/* L holds the land values everybody starts with; lm is an edge map with
    '   continents outlined in black and mountains outlined in white. */
    Private L(,) As Short ' MAXX, MAXY
    Private lm(,) As Short ' MAXX, MAXY
    Private change() As Integer ' = ones(6, 1)
    'UPGRADE_NOTE: step was upgraded to step_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private step_Renamed As Integer
    Private cl(,) As Byte ' MAXX, MAXY
    Private TCSIZE As Integer
    Private RCSIZE As Integer
    Private climkey() As Byte
    Private rn(,,) As Short ' MAXB, MAXX, MAXY
    Private tt(,,) As Short ' MAXB, MAXX, MAXY
    Private ts(,,) As Byte ' MAXB, MAXX, MAXY
    Private tmax, tmin As Integer
    Private tscale As Double
    Private TEMPSCALE As Integer
    Private wd(,,) As Byte
    Private pr(,,) As Byte
    Private fr(,,) As Byte
    Private fs(,) As Byte
    Private hl(,,) As Short
    Private p(,) As Short
    Private pm(,,) As Byte
    Private r(,) As Short
    Private rnscaled(,,) As Integer
    Private dndclim(,) As Byte

    ' Small versions of L() and tt() for pressure, wind, and rain
    Private Lsmall(,) As Short
    Private ttSmall(,,) As Short
    Private tsSmall(,,) As Byte

    Private mvarGradientMagnitude(,) As Integer

    Private mvarMaxStep As Integer
    'Private mvarZCoast As Long

    Public Property ZCoast() As Integer
        Get
            ZCoast = mvarZCoast
        End Get
        Set(ByVal Value As Integer)
            mvarZCoast = Value
        End Set
    End Property

    Public Property ZShelf() As Integer
        Get
            ZShelf = mvarZShelf
        End Get
        Set(ByVal Value As Integer)
            mvarZShelf = Value
        End Set
    End Property


    Public Property MaxStep() As Integer
        Get
            MaxStep = mvarMaxStep
        End Get
        Set(ByVal Value As Integer)
            mvarMaxStep = Value
        End Set
    End Property

    Public Property MaxRange() As Integer
        Get
            MaxRange = mvarMaxRange
        End Get
        Set(ByVal Value As Integer)
            mvarMaxRange = Value
        End Set
    End Property

    Public Property IcebergK() As Integer
        Get
            IcebergK = mvarIcebergK
        End Get
        Set(ByVal Value As Integer)
            mvarIcebergK = Value
        End Set
    End Property



    Public Property NearFlankDelta() As Integer
        Get
            NearFlankDelta = mvarNearFlankDelta
        End Get
        Set(ByVal Value As Integer)
            mvarNearFlankDelta = Value
        End Set
    End Property



    Public Property FlankDelta() As Integer
        Get
            FlankDelta = mvarFlankDelta
        End Get
        Set(ByVal Value As Integer)
            mvarFlankDelta = Value
        End Set
    End Property



    Public Property NearHeatEquatorDelta() As Integer
        Get
            NearHeatEquatorDelta = mvarNearHeatEquatorDelta
        End Get
        Set(ByVal Value As Integer)
            mvarNearHeatEquatorDelta = Value
        End Set
    End Property



    Public Property HeatEquatorDelta() As Integer
        Get
            HeatEquatorDelta = mvarHeatEquatorDelta
        End Get
        Set(ByVal Value As Integer)
            mvarHeatEquatorDelta = Value
        End Set
    End Property



    Public Property FetchDelta() As Integer
        Get
            FetchDelta = mvarFetchDelta
        End Get
        Set(ByVal Value As Integer)
            mvarFetchDelta = Value
        End Set
    End Property



    Public Property MountainDelta() As Integer
        Get
            MountainDelta = mvarMountainDelta
        End Get
        Set(ByVal Value As Integer)
            mvarMountainDelta = Value
        End Set
    End Property



    Public Property LandDelta() As Single
        Get
            LandDelta = mvarLandDelta
        End Get
        Set(ByVal Value As Single)
            mvarLandDelta = Value
        End Set
    End Property



    Public Property RainConst() As Integer
        Get
            RainConst = mvarRainConst
        End Get
        Set(ByVal Value As Integer)
            mvarRainConst = Value
        End Set
    End Property



    Public Property MaxFetch() As Integer
        Get
            MaxFetch = mvarMaxFetch
        End Get
        Set(ByVal Value As Integer)
            mvarMaxFetch = Value
        End Set
    End Property



    Public Property BarSep() As Integer
        Get
            BarSep = mvarBarSep
        End Get
        Set(ByVal Value As Integer)
            mvarBarSep = Value
        End Set
    End Property



    Public Property LHMax() As Integer
        Get
            LHMax = mvarLHMax
        End Get
        Set(ByVal Value As Integer)
            mvarLHMax = Value
        End Set
    End Property



    Public Property LHMin() As Integer
        Get
            LHMin = mvarLHMin
        End Get
        Set(ByVal Value As Integer)
            mvarLHMin = Value
        End Set
    End Property



    Public Property LLMax() As Integer
        Get
            LLMax = mvarLLMax
        End Get
        Set(ByVal Value As Integer)
            mvarLLMax = Value
        End Set
    End Property



    Public Property LLMin() As Integer
        Get
            LLMin = mvarLLMin
        End Get
        Set(ByVal Value As Integer)
            mvarLLMin = Value
        End Set
    End Property



    Public Property LLThresh() As Integer
        Get
            LLThresh = mvarLLThresh
        End Get
        Set(ByVal Value As Integer)
            mvarLLThresh = Value
        End Set
    End Property



    Public Property LOThresh() As Integer
        Get
            LOThresh = mvarLOThresh
        End Get
        Set(ByVal Value As Integer)
            mvarLOThresh = Value
        End Set
    End Property



    Public Property OHMin() As Integer
        Get
            OHMin = mvarOHMin
        End Get
        Set(ByVal Value As Integer)
            mvarOHMin = Value
        End Set
    End Property



    Public Property OLMax() As Integer
        Get
            OLMax = mvarOLMax
        End Get
        Set(ByVal Value As Integer)
            mvarOLMax = Value
        End Set
    End Property



    Public Property OHMax() As Integer
        Get
            OHMax = mvarOHMax
        End Get
        Set(ByVal Value As Integer)
            mvarOHMax = Value
        End Set
    End Property



    Public Property OLMin() As Integer
        Get
            OLMin = mvarOLMin
        End Get
        Set(ByVal Value As Integer)
            mvarOLMin = Value
        End Set
    End Property



    Public Property OOThresh() As Integer
        Get
            OOThresh = mvarOOThresh
        End Get
        Set(ByVal Value As Integer)
            mvarOOThresh = Value
        End Set
    End Property



    Public Property OLThresh() As Integer
        Get
            OLThresh = mvarOLThresh
        End Get
        Set(ByVal Value As Integer)
            mvarOLThresh = Value
        End Set
    End Property



    Public Property OceanTemperatureRange() As Double
        Get
            OceanTemperatureRange = mvarOceanTemperatureRange
        End Get
        Set(ByVal Value As Double)
            mvarOceanTemperatureRange = Value
        End Set
    End Property



    Public Property OceanTemperatureMean() As Double
        Get
            OceanTemperatureMean = mvarOceanTemperatureMean
        End Get
        Set(ByVal Value As Double)
            mvarOceanTemperatureMean = Value
        End Set
    End Property




    Public Property LandTemperatureRange() As Double
        Get
            LandTemperatureRange = mvarLandTemperatureRange
        End Get
        Set(ByVal Value As Double)
            mvarLandTemperatureRange = Value
        End Set
    End Property



    Public Property LandTemperatureMean() As Double
        Get
            LandTemperatureMean = mvarLandTemperatureMean
        End Get
        Set(ByVal Value As Double)
            mvarLandTemperatureMean = Value
        End Set
    End Property



    Public Property EccPhase() As Double
        Get
            EccPhase = mvarEccPhase
        End Get
        Set(ByVal Value As Double)
            mvarEccPhase = Value
        End Set
    End Property



    Public Property eccentricity() As Double
        Get
            eccentricity = mvarEccentricity
        End Get
        Set(ByVal Value As Double)
            mvarEccentricity = Value
        End Set
    End Property



    Public Property Tilt() As Double
        Get
            Tilt = mvarTilt
        End Get
        Set(ByVal Value As Double)
            mvarTilt = Value
        End Set
    End Property



    Public Property BSize() As Integer
        Get
            BSize = mvarBSize
        End Get
        Set(ByVal Value As Integer)
            mvarBSize = Value
            ReDim tt(mvarBSize, mvarXSize, mvarYSize)
            ReDim ttSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim tsSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim Lsmall(mvarSmallXSize, mvarSmallYSize)
            ReDim ts(mvarBSize, mvarXSize, mvarYSize)
            ReDim wd(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim pr(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim rn(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim pm(mvarBSize, mvarSmallXSize, mvarSmallYSize)
        End Set
    End Property




    Public Property YSize() As Integer
        Get
            YSize = mvarYSize
        End Get
        Set(ByVal Value As Integer)
            mvarYSize = Value
            ReDim cl(mvarXSize, mvarYSize)
            ReDim L(mvarXSize, mvarYSize)
            ReDim lm(mvarXSize, mvarYSize)
            ReDim tt(mvarBSize, mvarXSize, mvarYSize)
            ReDim ts(mvarBSize, mvarXSize, mvarYSize)
            ReDim tsSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim ttSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim Lsmall(mvarSmallXSize, mvarSmallYSize)
            ReDim wd(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim pr(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim rn(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim fr(2, mvarSmallXSize, mvarSmallYSize)
            ReDim fs(mvarSmallXSize, mvarSmallYSize)
            ReDim hl(2, mvarSmallXSize, mvarSmallYSize)
            ReDim p(mvarSmallXSize, mvarSmallYSize)
            ReDim pm(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim r(mvarSmallXSize, mvarSmallYSize)
        End Set
    End Property



    Public Property XSize() As Integer
        Get
            XSize = mvarXSize
        End Get
        Set(ByVal Value As Integer)
            mvarXSize = Value


            ReDim cl(mvarXSize, mvarYSize)
            ReDim L(mvarXSize, mvarYSize)
            ReDim lm(mvarXSize, mvarYSize)
            ReDim tt(mvarBSize, mvarXSize, mvarYSize)
            ReDim ts(mvarBSize, mvarXSize, mvarYSize)
            ReDim tsSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim ttSmall(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim Lsmall(mvarSmallXSize, mvarSmallYSize)
            ReDim wd(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim pr(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim rn(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim fr(2, mvarSmallXSize, mvarSmallYSize)
            ReDim fs(mvarSmallXSize, mvarSmallYSize)
            ReDim hl(2, mvarSmallXSize, mvarSmallYSize)
            ReDim p(mvarSmallXSize, mvarSmallYSize)
            ReDim pm(mvarBSize, mvarSmallXSize, mvarSmallYSize)
            ReDim r(mvarSmallXSize, mvarSmallYSize)
        End Set
    End Property


    Public Sub LetDEM(ByRef vData(,) As Short)
        mvarXSize = UBound(vData, 1)
        YSize = UBound(vData, 2)

        L = VB6.CopyArray(vData)

        MaxAlt = 0

        Dim i As Integer
        Dim j As Integer
        For i = 0 To UBound(L, 1)
            For j = 0 To UBound(L, 2)
                If L(i, j) > MaxAlt Then MaxAlt = L(i, j)
            Next j
        Next i

        mvarGradientMagnitude = QuickGradient2(L)

        'MaxAlt = MaxAlt * 133.333333333333
        'frmTec.StatusBar1.Panels(4).Text = "Max elevation = " & Format(MaxAlt, "##,##0") & " ft"
        'frmTec.lblMinVal.Caption = "0 ft"
        'frmTec.lblMaxVal.Caption = Format(MaxAlt, "##,##0") & " ft"
    End Sub
    Public Function GetDEM() As Short()
        GetDEM = VB6.CopyArray(L)
    End Function

    Public Function GetTemperatureMap() As Short(,,)
        GetTemperatureMap = VB6.CopyArray(tt)
    End Function

    Public Function GetClimateMap() As Byte(,)
        GetClimateMap = VB6.CopyArray(cl)
    End Function

    Public Function GetWindMap() As Byte(,,)
        GetWindMap = VB6.CopyArray(wd)
    End Function

    Public Function GetRainMap() As Short(,,)
        GetRainMap = VB6.CopyArray(rn)
    End Function

    Public Function GetPressureMap() As Byte(,,)
        GetPressureMap = VB6.CopyArray(pr)
    End Function

    Public Function GetDnDClimMap() As Byte(,)
        GetDnDClimMap = dndclim
    End Function

    Public Sub ScaleSmall()
        Dim ratio, i1, i, j, j1, buf As Integer
        Dim averageval As Single

        ratio = mvarXSize / mvarSmallXSize

        ' L to LSmall
        For i = 0 To mvarSmallXSize - 1
            For j = 0 To mvarSmallYSize - 1
                averageval = 0
                For i1 = 0 To ratio - 1
                    For j1 = 0 To ratio - 1
                        'If L(i + i1, j + j1) <> 0 Then

                        averageval = averageval + L(i * ratio + i1, j * ratio + j1)
                        'End If
                    Next j1
                Next i1
                Lsmall(i, j) = (averageval / (ratio ^ 2)) / (MAXMOUNTAINHEIGHT * 100.0# / 255)
            Next j
        Next i

        ' tt
        For buf = 0 To mvarBSize
            For i = 0 To mvarSmallXSize - 1
                For j = 0 To mvarSmallYSize - 1
                    averageval = 0
                    For i1 = 0 To ratio - 1
                        For j1 = 0 To ratio - 1
                            averageval = averageval + tt(buf, i * ratio + i1, j * ratio + j1)
                        Next j1
                    Next i1
                    ttSmall(buf, i, j) = averageval / (ratio ^ 2)
                Next j
            Next i
        Next buf

        ' ts
        For buf = 0 To mvarBSize
            For i = 0 To mvarSmallXSize - 1
                For j = 0 To mvarSmallYSize - 1
                    averageval = 0
                    For i1 = 0 To ratio - 1
                        For j1 = 0 To ratio - 1
                            averageval = averageval + ts(buf, i * ratio + i1, j * ratio + j1)
                        Next j1
                    Next i1
                    tsSmall(buf, i, j) = averageval / (ratio ^ 2)
                Next j
            Next i
        Next buf


    End Sub


    Public Sub RunModel()
        ComputeHeat()

        ScaleSmall()

        ComputePressure()
        ComputeWind()
        ComputeRain()

        ComputeClimate()



    End Sub






    

    Private Sub range(ByRef rr(,) As Short)
        'void range (char rr[MAXX][MAXY])  {
        '   This function is called by a number of climate routines.  It takes an
        '   input array with blobs of -1's on a background of 0's.  The function winds
        '   up replacing each 0 with the distance from that square to the nearest -1.
        '   The function onerange() does all the work, but it will not compute ranges
        '   greater than MAXRANGE.  Therefore, after onerange() is called, any remaining
        '   0 values must be replaced with MAXRANGE, indicating that that square is
        '   "very far" from any -1 value.

        Dim i, j As Integer

        onerange(rr)
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If rr(i, j) = 0 Then rr(i, j) = mvarMaxRange
            Next i
        Next j
    End Sub

    Private Sub range3D(ByRef rr(,,) As Short, ByRef layer As Integer)
        'void range (char rr[MAXX][MAXY])  {
        '   This function is called by a number of climate routines.  It takes an
        '   input array with blobs of -1's on a background of 0's.  The function winds
        '   up replacing each 0 with the distance from that square to the nearest -1.
        '   The function onerange() does all the work, but it will not compute ranges
        '   greater than MAXRANGE.  Therefore, after onerange() is called, any remaining
        '   0 values must be replaced with MAXRANGE, indicating that that square is
        '   "very far" from any -1 value.

        Dim i, j As Integer

        onerange3d(rr, layer)
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If rr(layer, i, j) = 0 Then rr(layer, i, j) = mvarMaxRange
            Next i
        Next j
    End Sub

    Private Function onerange(ByRef rr(,) As Short) As Integer
        'int onerange (char rr[MAXX][MAXY])  {
        '   /* This routine consists of a loop.  Each time through the loop, every
        '   square is checked.  If the square is zero, it has not yet been updated.
        '   In that case, look to see if any adjacent squares were previously updated
        '   (or if they were initialized to -1).  If so, set the square to the current
        '   distance value, which happens to be identical to the outer loop variable.
        '   If, after one loop iteration, no squares have been updated, the matrix
        '   must be completely updated.  Stop.  To keep down run-time, a maximum
        '   distance value, MAXRANGE, is used as the terminating loop value. */

        Dim k, j, i, X, keepgo As Integer

        For k = 1 To mvarMaxRange
            System.Windows.Forms.Application.DoEvents()
            keepgo = 0
            For j = 0 To mvarSmallYSize
                For i = 0 To mvarSmallXSize
                    If rr(i, j) = 0 Then
                        keepgo = 1
                        If i <> 0 Then
                            X = rr(i - 1, j)
                        Else
                            X = rr(mvarSmallXSize - 1, j)
                        End If
                        If (X And (X <> k)) Then rr(i, j) = k

                        If i < mvarSmallXSize - 1 Then
                            X = rr(i + 1, j)
                        Else
                            X = rr(0, j)
                        End If

                        If (X And (X <> k)) Then rr(i, j) = k
                        If (j < mvarSmallYSize - 1) Then
                            X = rr(i, j + 1)
                            If (X And (X <> k)) Then rr(i, j) = k
                        End If
                        If (j) Then
                            X = rr(i, j - 1)
                            If (X And (X <> k)) Then rr(i, j) = k
                        End If
                    End If
                Next i
            Next j
            If keepgo = 0 Then
                onerange = 0
                Exit Function
            End If
        Next k
        onerange = 0
    End Function


    Private Function onerange3d(ByRef rr(,,) As Short, ByRef layer As Integer) As Integer
        'int onerange (char rr[MAXX][MAXY])  {
        '   /* This routine consists of a loop.  Each time through the loop, every
        '   square is checked.  If the square is zero, it has not yet been updated.
        '   In that case, look to see if any adjacent squares were previously updated
        '   (or if they were initialized to -1).  If so, set the square to the current
        '   distance value, which happens to be identical to the outer loop variable.
        '   If, after one loop iteration, no squares have been updated, the matrix
        '   must be completely updated.  Stop.  To keep down run-time, a maximum
        '   distance value, MAXRANGE, is used as the terminating loop value. */

        Dim k, j, i, X, keepgo As Integer

        For k = 1 To mvarMaxRange
            System.Windows.Forms.Application.DoEvents()
            keepgo = 0
            For j = 0 To mvarSmallYSize
                For i = 0 To mvarSmallXSize
                    If rr(layer, i, j) = 0 Then
                        keepgo = 1
                        If i <> 0 Then
                            X = rr(layer, i - 1, j)
                        Else
                            X = rr(layer, mvarSmallXSize - 1, j)
                        End If
                        If (X And (X <> k)) Then rr(layer, i, j) = k

                        If i < mvarSmallXSize - 1 Then
                            X = rr(layer, i + 1, j)
                        Else
                            X = rr(layer, 0, j)
                        End If

                        If (X And (X <> k)) Then rr(layer, i, j) = k
                        If (j < mvarSmallYSize - 1) Then
                            X = rr(layer, i, j + 1)
                            If (X And (X <> k)) Then rr(layer, i, j) = k
                        End If
                        If (j) Then
                            X = rr(layer, i, j - 1)
                            If (X And (X <> k)) Then rr(layer, i, j) = k
                        End If
                    End If
                Next i
            Next j
            If keepgo = 0 Then
                onerange3d = 0
                Exit Function
            End If
        Next k
        onerange3d = 0
    End Function


    Private Sub status(ByRef n As Integer, ByRef i As Integer)
        Dim title As Object

        'UPGRADE_WARNING: Array has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        'UPGRADE_WARNING: Couldn't resolve default property of object title. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        title = New Object() {"", "Heat", "Pressure", "Wind", "Rain", "Climate"}
        'frmTec.StatusBar1.Panels(1).Text = title(n) & " buffer " & CStr(i)

    End Sub


    ' CLIMATE.C

    Public Function CreateHiResTrueColor(ByRef topo(,) As Integer) As Integer()
        Dim ou(,) As Integer
        Dim k, i, j, L As Integer
        'Dim temprow, tempcol As Integer
        Dim scalefac As Integer
        Dim maxtemp, buf, avetemp As Integer

        ReDim ou(UBound(topo, 1), UBound(topo, 2))
        scalefac = UBound(topo, 1) / UBound(cl, 1)

        For i = 0 To UBound(cl, 1) - 1
            For j = 0 To UBound(cl, 2) - 1
                For k = 0 To scalefac - 1
                    For L = 0 To scalefac - 1

                        If (topo(i * scalefac + k, j * scalefac + L) >= mvarZCoast And cl(i, j) <> modTecGlobals.Climate_E.C_OCEAN And cl(i, j) <> modTecGlobals.Climate_E.C_OCEANICE) Or (topo(i * scalefac + k, j * scalefac + L) < mvarZCoast And (cl(i, j) = modTecGlobals.Climate_E.C_OCEAN Or cl(i, j) = modTecGlobals.Climate_E.C_OCEANICE)) Then

                            ou(i * scalefac + k, j * scalefac + L) = cl(i, j)

                        ElseIf topo(i * scalefac + k, j * scalefac + L) >= mvarZCoast And cl(i, j) = modTecGlobals.Climate_E.C_OCEAN Or cl(i, j) = modTecGlobals.Climate_E.C_OCEANICE Then

                            ou(i * scalefac + k, j * scalefac + L) = modTecGlobals.Climate_E.C_DESERT ' for beaches

                        Else ' topo < mvarZCoast, check for climate type
                            avetemp = 0
                            maxtemp = -32000
                            For buf = 0 To mvarBSize - 1
                                If tt(buf, i, j) > maxtemp Then maxtemp = tt(buf, i, j)
                                avetemp = avetemp + tt(buf, i, j)
                            Next buf
                            avetemp = avetemp / mvarBSize

                            If avetemp < 2640 Or maxtemp < 2730 Then
                                ou(i * scalefac + k, j * scalefac + L) = modTecGlobals.Climate_E.C_OCEANICE
                            Else
                                ou(i * scalefac + k, j * scalefac + L) = modTecGlobals.Climate_E.C_OCEAN
                            End If

                        End If

                    Next L
                Next k
            Next j
        Next i
        CreateHiResTrueColor = VB6.CopyArray(ou)
    End Function

    Private Sub ComputeClimate()
        '   The outer loop looks at each square.  If it is ocean, the climate will
        '   be ocean unless the temperature is below ICEBREGK degrees all year round.
        '   If it is land, then the average rainfall and temperature (in Farenheit) are
        '   computed for the square.  If the square is mountain, it is colder; the
        '   temperature is decreased.  These two figures are then turned into array
        '   indices by using the tempcut and raincut parameter vectors.  The climate
        '   for the square is then simply a table lookup.  Finally, the array is printed
        '   if desired.

        Dim j, i, buf As Integer
        Dim avetemp As Integer
        Dim lat, lon, Celsius As Double
        Dim MinTemp_C, MaxTemp_C, TempRange_C As Double
        Dim RainRange_cm, MinRain_cm, MaxRain_cm, AnnualRain_cm As Double
        Dim RainFact As Double
        Dim DryestMonth, WettestMonth As Byte

        Dim ratio As Integer


        ReDim dndclim(UBound(cl, 1), UBound(cl, 2))

        MinTemp_C = 5000
        MaxTemp_C = -5000
        TempRange_C = 0
        MinRain_cm = 32768
        MaxRain_cm = 0
        RainRange_cm = 0
        AnnualRain_cm = 0

        ratio = mvarXSize / mvarSmallXSize

        For j = 0 To mvarYSize
            For i = 0 To mvarXSize
                lat = PI / 2 - (2 * PI * j / mvarYSize) * 180 / PI + 90
                lon = PI - (PI * 2 * i) / mvarXSize * 180 / PI + 180

                If L(i, j) <= mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255 Then ' ocean
                    avetemp = 0
                    MaxTemp_C = 0
                    MinTemp_C = 1200000
                    For buf = 0 To mvarBSize - 1
                        avetemp = avetemp + tt(buf, i, j) / TEMPSCALE
                        If MaxTemp_C < tt(buf, i, j) / TEMPSCALE Then MaxTemp_C = tt(buf, i, j) / TEMPSCALE
                        If MinTemp_C > tt(buf, i, j) / TEMPSCALE Then MinTemp_C = tt(buf, i, j) / TEMPSCALE
                    Next buf
                    avetemp = avetemp / mvarBSize

                    If avetemp <= mvarIcebergK Or MaxTemp_C <= FREEZING Then
                        cl(i, j) = modTecGlobals.Climate_E.C_OCEANICE
                        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_OCEANICE
                    Else
                        cl(i, j) = modTecGlobals.Climate_E.C_OCEAN
                        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_AQUATIC
                        If MinTemp_C > FREEZING + 18 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_AQUATIC
                    End If
                    If MinTemp_C <= -5 + FREEZING And MaxTemp_C >= 10 + FREEZING Then
                        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_AQUATIC
                    End If
                Else ' land or mountain
                    ' calculate the temperature and rainfall stats for this spot
                    avetemp = 0
                    AnnualRain_cm = 0
                    MinTemp_C = 5000
                    MaxTemp_C = -275
                    For buf = 0 To mvarBSize - 1
                        'fahrenheit = (9 / 5) * (tt(buf, i, j) / TEMPSCALE - FREEZING#) + 32
                        Celsius = tt(buf, i, j) / TEMPSCALE - FREEZING

                        If Celsius < MinTemp_C Then MinTemp_C = Celsius
                        If Celsius > MaxTemp_C Then MaxTemp_C = Celsius
                        AnnualRain_cm = AnnualRain_cm + rn(buf, i / ratio, j / ratio) * 12 / mvarBSize

                        If rn(buf, i / ratio, j / ratio) * 12 / mvarBSize < MinRain_cm Then
                            MinRain_cm = rn(buf, i / ratio, j / ratio) * 12 / mvarBSize
                            DryestMonth = buf
                        End If
                        If rn(buf, i / ratio, j / ratio) * 12 / mvarBSize > MaxRain_cm Then
                            MaxRain_cm = rn(buf, i / ratio, j / ratio) * 12 / mvarBSize
                            WettestMonth = buf
                        End If
                        avetemp = avetemp + Celsius
                    Next buf


                    TempRange_C = MaxTemp_C - MinTemp_C
                    RainRange_cm = MaxRain_cm - MinRain_cm
                    avetemp = avetemp / mvarBSize

                    If MaxRain_cm > 0 Then
                        RainFact = 100.0# * RainRange_cm / MaxRain_cm
                    Else
                        RainFact = 0
                    End If

                    ' A moist tropical climates
                    If MinTemp_C >= 18 Then ' A
                        Select Case AnnualRain_cm
                            Case Is < 25 ' B climates
                                cl(i, j) = modTecGlobals.Climate_E.C_DESERT
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_DESERT
                            Case 25 To 45 ' plains
                                cl(i, j) = modTecGlobals.Climate_E.C_SAVANA
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_PLAIN
                                If CheckForDnDHills(i, j) = True Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_HILL
                                If L(i, j) >= 4000 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_MOUNTAIN
                            Case Is > 45 ' lots of rain.  Either forest or monsoon
                                'Select Case RainFact
                                '    Case Is < 60
                                cl(i, j) = modTecGlobals.Climate_E.C_JUNGLE
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_FOREST
                                '    Case Else
                                '        If MinRain_cm > 0.3 Then
                                '            cl(i, j) = Climate_E.C_TROPICWETDRY
                                '            dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_FOREST
                                '        Else
                                '            cl(i, j) = Climate_E.C_MONSOON
                                '            dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_FOREST
                                '        End If
                                'End Select
                                If L(i, j) >= mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255 And L(i, j) < (mvarZCoast + 1) * MAXMOUNTAINHEIGHT * 100.0# / 255 Then

                                    cl(i, j) = modTecGlobals.Climate_E.C_SWAMP
                                    dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_WARM_SWAMP
                                End If
                        End Select

                    ElseIf MinTemp_C >= -5 And MinTemp_C < 18 And MaxTemp_C >= 10 Then
                        Select Case AnnualRain_cm
                            Case Is < 25 ' desert
                                cl(i, j) = modTecGlobals.Climate_E.C_DESERT
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_DESERT
                            Case 25 To 40 ' steppes and plains
                                cl(i, j) = modTecGlobals.Climate_E.C_STEPPE
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_PLAIN
                                If CheckForDnDHills(i, j) = True Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_HILL
                                If L(i, j) >= 4000 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_MOUNTAIN
                            Case Else


                                'Select Case RainFact
                                '   Case Is < 60
                                cl(i, j) = modTecGlobals.Climate_E.C_DECID
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_FOREST
                                'Case Else '60 To 88
                                'Select Case DryestMonth
                                'Case MONTH_E.JANUARY To MONTH_E.MARCH
                                'cl(i, j) = Climate_E.C_GRASSLAND ' Dry winter?
                                'dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_PLAIN
                                'Case MONTH_E.APRIL To MONTH_E.SEPTEMBER
                                '    cl(i, j) = Climate_E.C_MEDITERRANEAN ' Dry summer?
                                '    dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_PLAIN
                                'Case MONTH_E.OCTOBER To MONTH_E.DECEMBER
                                '    cl(i, j) = Climate_E.C_GRASSLAND ' Dry winter?
                                '    dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_PLAIN
                                'End Select
                                'End Select


                                If L(i, j) >= mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255 And L(i, j) < (mvarZCoast + 1) * MAXMOUNTAINHEIGHT * 100.0# / 255 Then

                                    dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_TEMP_SWAMP
                                    cl(i, j) = modTecGlobals.Climate_E.C_SWAMP
                                End If
                        End Select

                    ElseIf MinTemp_C <= -5 And MaxTemp_C >= 10 Then
                        ' D moist continental mid-latitude climates, cold winter
                        Select Case AnnualRain_cm
                            Case Is < 25
                                cl(i, j) = modTecGlobals.Climate_E.C_DESERT
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_DESERT
                            Case 25 To 40
                                cl(i, j) = modTecGlobals.Climate_E.C_STEPPE
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_PLAIN
                                If CheckForDnDHills(i, j) = True Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_HILL
                                If L(i, j) >= 4000 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_MOUNTAIN
                            Case Else
                                'Select Case RainFact
                                '    Case Is < 75
                                cl(i, j) = modTecGlobals.Climate_E.C_BOREAL
                                dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_FOREST
                                '    Case Else
                                '        cl(i, j) = Climate_E.C_COLDDRYWINTER
                                '        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_FOREST
                                'End Select
                                If L(i, j) >= mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255 And L(i, j) < (mvarZCoast + 1) * MAXMOUNTAINHEIGHT * 100.0# / 255 Then

                                    dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_SWAMP
                                    cl(i, j) = modTecGlobals.Climate_E.C_SWAMP
                                End If
                        End Select
                    ElseIf MaxTemp_C >= 0 And MaxTemp_C <= 10 Then
                        ' Tundra
                        cl(i, j) = modTecGlobals.Climate_E.C_TUNDRA
                        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_DESERT

                        If L(i, j) >= 4000 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_MOUNTAIN
                        If CheckForDnDHills(i, j) = True Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_HILL

                    ElseIf MaxTemp_C < 0 Then
                        ' Ice cap
                        cl(i, j) = modTecGlobals.Climate_E.C_LANDICE
                        dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_PLAIN
                        If CheckForDnDHills(i, j) = True Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_HILL
                        If L(i, j) >= 4000 Then dndclim(i, j) = modTecGlobals.DNDCLIMATE_E.D_COLD_MOUNTAIN
                    Else
                        MsgBox("Cannot compute climate: " & Chr(10) & "T=[" & CStr(MinTemp_C) & ":" & CStr(MaxTemp_C) & "]" & Chr(10) & "P=" & CStr(AnnualRain_cm))
                    End If

                End If
            Next i
        Next j

    End Sub





    ' HEAT.C
    '   The input array is l, from main.c; lm is used by DrawTemperatureMap().  The output
    '   array is ts, containing temperatures.  Array t is an unscaled copy of the
    '   temperatures; tmin and tmax are used for scaling, and tscale is the
    '   computed scale factor.  To convert the unscaled temperatures to degrees K,
    '   divide by TEMPSCALE.

    Private Sub ComputeHeat()
        ' This is the main routine for computing temperatures.  After getheat()
        ' is called to do all the work, this routine takes the ints from t and
        ' finds the smallest and largest values.  These are used to compute a scale
        ' factor, tscale; the arrays ts are then filled with scaled values.  Finally,
        ' putmat() from main.c is called if needed to print results.

        Dim j, i, buf As Integer

        GetHeat()
        tmin = 32000
        tmax = 0

        'frmTec.StatusBar1.Panels(1).Text = "Scaling heat"
        ' Find minimum and maximum across all buffers
        For buf = 0 To mvarBSize
            System.Windows.Forms.Application.DoEvents()
            For i = 0 To mvarXSize
                For j = 0 To mvarYSize
                    If (tt(buf, i, j) < tmin) Then tmin = tt(buf, i, j)
                    If (tt(buf, i, j) > tmax) Then tmax = tt(buf, i, j)
                Next j
            Next i
        Next buf

        ' Compute scale; for every buffer, adjust tt, then fill ts from t
        tscale = 254.0# / (CDbl(tmax - tmin)) ' for colormap

        For buf = 0 To mvarBSize
            For i = 0 To mvarXSize
                For j = 0 To mvarYSize
                    ts(buf, i, j) = (tt(buf, i, j) - tmin) * tscale
                Next j
            Next i
            System.Windows.Forms.Application.DoEvents()
        Next buf

    End Sub

    Private Sub GetHeat()
        '   This function does all the work for computing temperatures.  The outermost
        '   loop goes through each row of the output array once, computing all buffers
        '   at the same time.  The loop has two inner loops: first, tland and tsea are
        '   filled with the temperatures found far inland and far at sea for each buffer.
        '   In the second loop, the weight function for each point in the latitude line
        '   is computed and the temperature is found for each buffer.

        Dim j, i, buf As Integer
        Dim delth, fact, xs, sscl, lat, lscl, xl, theta, phase As Double
        Dim tland() As Double
        Dim tsea() As Double
        Dim temperature As Single

        Dim lngLand As Integer

        ReDim tland(mvarBSize)
        ReDim tsea(mvarBSize)

        lscl = DEG2RAD * 180.0# / (90.0# + mvarTilt)
        sscl = DEG2RAD * 180.0# / (90.0# + mvarTilt)
        delth = 2.0# * PI / CDbl(mvarBSize)

        tmin = 32000
        tmax = -500

        For j = 0 To mvarYSize
            status(modTecGlobals.MainType_E.M_HEAT, j)
            System.Windows.Forms.Application.DoEvents()
            lat = 90.0# - 180.0# * CDbl(j) / CDbl(mvarYSize)
            theta = 0

            ' Calculate the base temperature based on tilt and latitude
            For buf = 0 To mvarBSize
                phase = theta + mvarEccPhase

                If (phase > 2.0# * PI) Then
                    phase = phase - (2 * PI)
                End If

                fact = (1.0# + mvarEccentricity * System.Math.Cos(phase)) * TEMPSCALE

                ' x is the effective latitude in radians due to the tilt
                xl = (lat + System.Math.Cos(theta) * mvarTilt) * lscl
                tland(buf) = (mvarLandTemperatureMean + mvarLandTemperatureRange / 2 * System.Math.Cos(xl)) * fact

                xs = (lat + System.Math.Cos(theta) * mvarTilt) * sscl
                tsea(buf) = (mvarOceanTemperatureMean + mvarOceanTemperatureRange * System.Math.Cos(xs)) * fact

                theta = theta + delth
            Next buf

            ' Calculate the smoothing
            For i = 0 To mvarXSize

                For buf = 0 To mvarBSize
                    lngLand = CountLand(i, j)


                    tt(buf, i, j) = tsea(buf) * (1 - lngLand / 198) + tland(buf) * (lngLand / 198)


                    ' factor in altitude
                    If L(i, j) > mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255 Then

                        ' back to Fahrenheit
                        temperature = (tt(buf, i, j) / TEMPSCALE - FREEZING) * (9 / 5) + 32

                        ' do the elevation math
                        temperature = temperature - 0.0026 * (L(i, j) - (mvarZCoast * MAXMOUNTAINHEIGHT * 100.0# / 255))

                        ' back to scaled kelvins
                        tt(buf, i, j) = ((temperature - 32) * (5 / 9) + FREEZING) * TEMPSCALE

                    End If


                    If (tt(buf, i, j) < tmin) Then tmin = tt(buf, i, j)
                    If (tt(buf, i, j) > tmax) Then tmax = tt(buf, i, j)

                Next buf

            Next i
        Next j





    End Sub

    Private Function CountLand(ByRef X As Integer, ByRef y As Integer) As Integer
        '   Called by getheat() for each square, this function looks in a 24 wide
        '   by 11 high box and counts the number of land squares found there.  It
        '   compensates for y values off the map, and wraps x values around.   The
        '   answer is returned.

        Dim i0, jmax, sum, jmin, j1, i1 As Integer
        sum = 0
        jmin = y - 5
        If jmin < 0 Then jmin = 0
        jmax = y + 5
        If jmax >= mvarYSize Then jmax = mvarYSize - 1



        For j1 = jmin To jmax
            For i0 = -11 To 12
                i1 = i0 + X
                If i1 < 0 Then i1 = i1 + mvarXSize
                If i1 >= mvarXSize Then i1 = i1 - mvarXSize
                If L(i1, j1) >= mvarZCoast * 133.33333333333 Then sum = sum + 1
            Next i0
        Next j1
        CountLand = sum
    End Function


    '' PRESSURE
    '
    '/* These are the data arrays required: l is an input array defining the
    '   ocean, land and mountain areas; ts is the temperature array computed
    '   by the functions in heat.c.  Array pr is filled by ocean(), land() and
    '   heateq(), below; PR_HIGH indicates a high pressure zone, PR_LOW indicates
    '   a low, and PR_HEQ indicates the heat equator, a low zone.  Array pm is
    '   the output array for this file, and it contains an edge map which has
    '   edges in color 1 surrounding lows and color 0 around highs.  Array r is
    '   temporary storage for local calls to range() in main.c. */

    Private Sub ComputePressure()
        'void ComputePressure () {
        '   The main routine for this file.  It just calls the four routines
        '   which do all the work.  Ocean() finds pressure extremes on the ocean;
        '   land() does the same for land; heateq() defines the heat equator, and
        '   SetPressureMap() computes pm[][] from pr[][].

        Dim buf As Integer

        For buf = 0 To mvarBSize
            status(modTecGlobals.MainType_E.M_PRESS, buf)
            PressureOceanHighLow(buf)
            PressureLandHighLow(buf)

            System.Windows.Forms.Application.DoEvents()

            FindHeatEquator(buf)
            SetPressureMap(buf)
            'DrawPressureMap(buf)
            System.Windows.Forms.Application.DoEvents()
        Next buf

    End Sub

    'Public Sub DrawPressureMap(ByRef n As Integer)
    '	' This function calls draw with the right arguments to display pressure
    '	DrawShortByte_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_CORN, Lsmall, pm, n)
    'End Sub

    Private Sub PressureOceanHighLow(ByRef buf As Integer)
        '   Determine ocean highs and lows.  An ocean high or low must occur over
        '   ocean, far away from major land masses.  Two calls to range() are made
        '   to find the qualifying ocean areas; then temperature criteria are used
        '   to select the actual pressure zones.
        Dim j, i, X As Integer

        ' Set r to the distance on land from the coast.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If Lsmall(i, j) <> 0 Then
                    r(i, j) = 0
                Else
                    r(i, j) = -1
                End If
            Next i
        Next j
        range(r)

        ' Initialize r to contain blobs on land which are at least OLTHRESH squares
        ' away from the coast.  Then set r to the distance from these.  The result
        ' in r is the distance from the nearest big piece of land (ignoring
        ' islands).
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If r(i, j) > mvarOLThresh Then
                    r(i, j) = -1
                Else
                    r(i, j) = 0
                End If
            Next i
        Next j
        range(r)

        ' For each array element, if it is at least OOTHRESH squares from the
        ' nearest big piece of land, it might be the center of an ocean pressure
        ' zone.  The pressure zones are defined by temperature ranges; if the
        ' temperature in ts is between OLMIN and OLMAX, a low is recorded, while
        ' if the temperature is between OHMIN and OHMAX, a high is recorded.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                pr(buf, i, j) = 0
                X = tsSmall(buf, i, j)
                If (r(i, j) > mvarOOThresh) Then
                    If ((X >= mvarOLMin) And (X <= mvarOLMax)) Then
                        pr(buf, i, j) = modTecGlobals.Pressure_E.PR_LOW
                    End If
                    If ((X >= mvarOHMin) And (X <= mvarOHMax)) Then
                        pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HIGH
                    End If
                End If
            Next i
        Next j
    End Sub


    Private Sub PressureLandHighLow(ByRef buf As Integer)
        '   This function is simply the complement of ocean(): it finds land highs
        '   and lows.  A land high or low must occur over land, far from major oceans.
        '   Two calls to range() are made to find the qualifying land areas; then
        '   temperature criteria are used to select the actual pressure zones.

        Dim j, i, X As Integer

        ' Set r to distance on water from coast.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If Lsmall(i, j) <> 0 Then
                    r(i, j) = -1
                Else
                    r(i, j) = 0
                End If
            Next i
        Next j
        range(r)

        ' Initialize r to contain blobs on ocean which are at least LOTHRESH
        ' squares away from the coast.  Then set r to the distance from these.  The
        ' result in r is the distance from the nearest ocean, ignoring lakes.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If r(i, j) > mvarLOThresh Then
                    r(i, j) = -1
                Else
                    r(i, j) = 0
                End If
            Next i
        Next j
        range(r)

        ' For each array element, if it is at least LLTHRESH squares from the
        ' nearest large ocean, it might be the center of a land pressure zone.
        ' The pressure zones are defined by temperature ranges; if the temperature
        ' in ts is between LLMIN and LLMAX, a low is recorded, while if the
        ' temperature is between LHMIN and LHMAX, a high is recorded.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                X = ts(buf, i, j)
                If r(i, j) > mvarLLThresh Then
                    If (X >= mvarLLMin) And (X <= mvarLLMax) Then
                        pr(buf, i, j) = modTecGlobals.Pressure_E.PR_LOW
                    End If
                    If (X >= mvarLHMin) And (X <= mvarLHMax) Then
                        pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HIGH
                    End If
                End If
            Next i
        Next j
    End Sub

    Private Sub FindHeatEquator(ByRef buf As Integer)
        '   This function finds the heat equator and marks it in pr.  For each
        '   vertical column of ts, the median position is found and marked.  To
        '   make the heat equator continuous, jlast is set to the position of the
        '   heat equator in the previous column; a connection is made in the present
        '   column to ensure continuity.
        Dim jlast, j, i, sum, jnext As Integer
        jlast = 0

        For i = 0 To mvarSmallXSize
            ' Find the total of the temperatures in this column
            sum = 0
            For j = 0 To mvarSmallYSize
                sum = sum + tsSmall(buf, i, j)
            Next j

            ' Step through the column again until the total so far is exactly
            ' half the total for the column.  This is the median position.
            sum = sum / 2
            For j = 0 To mvarSmallYSize
                If sum <= 0 Then Exit For
                sum = sum - tsSmall(buf, i, j)
            Next j

            ' Mark this position and remember it with jnext
            pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HEQ
            jnext = j

            ' For each column except the first (where i = 0), if the last heat
            ' equator is above this one, move upwards to it, marking each square,
            ' to ensure continuity; if below this one, move downwards to it.

            If (i <> 0) And (j > jlast) Then
                For j = j To jlast Step -1
                    pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HEQ
                Next j

            ElseIf (i <> 0) And (j < jlast) Then
                For j = j To jlast Step -1
                    pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HEQ
                Next j
            End If

            ' Remember this position for the next column.  Note that no check is
            ' done to ensure continuity at the wraparound point; this is bad.
            jlast = jnext
        Next i
    End Sub

    Private Sub SetPressureMap(ByRef buf As Integer)

        '   Setpm() is called after the above three functions have filled pr with
        '   the codes for high, low and heat equator.  The purpose of this function
        '   is to create an edge map surrounding lows with color 1 and highs with
        '   color 0.
        Dim k, i, j, col As Integer

        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                k = pr(buf, i, j)
                col = 0

                ' If not at the top edge, and if the pressure status here is not equal
                ' to the pressure status one square up, then put a horizontal line in
                ' this square.  The color is zero if there is a high here; if a low or
                ' heat equator (a type of low) is here, the color is one.
                If j <> 0 Then
                    If k <> pr(buf, i, j - 1) Then
                        If (k = modTecGlobals.Pressure_E.PR_HIGH) Or (pr(buf, i, j - 1) = modTecGlobals.Pressure_E.PR_HIGH) Then
                            col = LINE_0H
                        Else
                            col = LINE_1H
                        End If
                    End If
                End If

                ' Similarly, if not at the left edge, put a vertical line in the right
                ' color.  Notice that this color is OR'ed with the previous color.
                If i <> 0 Then
                    If k <> pr(buf, i - 1, j) Then
                        If (k = modTecGlobals.Pressure_E.PR_HIGH) Or (pr(buf, i - 1, j) = modTecGlobals.Pressure_E.PR_HIGH) Then
                            col = (col Or LINE_0V)
                        Else
                            col = (col Or LINE_1V)
                        End If
                    End If
                End If
                ' Set the square in the pm array to the resultant color
                pm(buf, i, j) = col
            Next i
        Next j
    End Sub


    '' RAIN
    ' The input data arrays are l and lm, from main.c, wd, from wind.c,
    ' and pr, from pressure.c.  Output arrays are rm and rn; fr and fs are
    ' used as temporary storage.


    Private Sub ComputeRain()
        '   This is the main rain computation function.  It calls the functions
        '   getfetch () and getrain () to do all the work for each buffer, then
        '   prints out the results if needed.
        Dim buf As Integer
        Dim i, j As Integer
        Dim rnscl As Double

        ReDim rnscaled(UBound(rn, 1), UBound(rn, 2), UBound(rn, 3))

        MinRain = 100000000.0#
        MaxRain = 0

        For buf = 0 To mvarBSize
            status(modTecGlobals.MainType_E.M_RAIN, buf)

            GetFetch(buf)

            GetRain(buf)

            rnscl = (MaxRain - MinRain) / 256

            For i = 0 To UBound(rn, 2)
                For j = 0 To UBound(rn, 3)
                    rnscaled(buf, i, j) = (rn(buf, i, j) - MinRain) * rnscl
                    If rnscaled(buf, i, j) < 0 Then rnscaled(buf, i, j) = 0
                    If rnscaled(buf, i, j) > 255 Then rnscaled(buf, i, j) = 255
                Next j
            Next i


            System.Windows.Forms.Application.DoEvents()
        Next buf


    End Sub


    Private Function FetchIncrement(ByRef X As Integer, ByRef y As Integer, ByRef dest As Integer) As Integer
        '   This is the workhorse function for getfetch(), below.  It is called
        '   several times per square.  It changes x to account for wraparound, so it
        '   won't work as a macro.  If y is out of range it does nothing, else it
        '   "marks" the new square in fr[dest] and increments fs to record the number
        '   of times the square has been marked.

        If (X = -1) Then
            X = mvarSmallXSize - 1
        ElseIf (X = mvarSmallXSize) Then
            X = 0
        ElseIf X > mvarSmallXSize Then
            X = X - mvarSmallXSize
        End If
        If (y = -1) Or (y = mvarSmallYSize) Then
            FetchIncrement = 0
            Exit Function
        End If
        If y >= mvarSmallYSize Then
            y = y - mvarSmallYSize
        End If

        fr(dest, X, y) = 1
        fs(X, y) = fs(X, y) + 1
        FetchIncrement = 0
    End Function

    Private Sub GetFetch(ByRef buf As Integer)
        ' "Fetch" is the term that describes how many squares a given wind line
        ' travels over water.  It measures how moist the wind is.  The algorithm to
        ' measure fetch looks like many simultaneous tree walks, where each water
        ' square is a root square, and every wind edge is a tree edge.  A counter
        ' for each square determines how many times that square is reached during
        ' the tree walks; that is the fetch.

        Dim src, j, i, k, dest As Integer

        ' Initialize the counter fs to zero.  Array fr, which records the
        ' list of active edges in the walks, is set so that all ocean squares
        ' are active.  Also, the result array rn is cleared.
        For i = 0 To mvarSmallXSize
            For j = 0 To mvarSmallYSize
                If L(i, j) <> 0 Then
                    fr(0, i, j) = 0
                Else
                    fr(0, i, j) = 1
                End If
                fs(i, j) = 0
                rn(buf, i, j) = 0
            Next j
        Next i

        ' Each time through the loop, each square is examined.  If it's
        ' active, disable the mark in the current time step (thus ensuring
        ' that when the buffers are flipped, the new destination is empty).
        ' If the square is a mountain, don't pass the mark, but instead add
        ' some amount to the square -- implementing rain shadows and rainy
        ' mountain squares.  Finally, for each of the eight cardinal
        ' directions, if there is wind blowing in that direction, carry a
        ' marker to that square using FetchIncrement(), above.

        For k = 0 To mvarMaxFetch
            src = k Mod 2
            dest = 1 - src
            For i = 0 To mvarSmallXSize
                For j = 0 To mvarSmallYSize
                    If fr(src, i, j) <> 0 Then
                        fr(src, i, j) = 0
                        If L(i, j) >= TREELINE Then
                            rn(buf, i, j) = rn(buf, i, j) + mvarMountainDelta
                        Else
                            Select Case wd(buf, i, j)
                                Case Northeast
                                    FetchIncrement(i + 1, j - 1, dest)
                                Case Northwest
                                    FetchIncrement(i - 1, j - 1, dest)
                                Case Southeast
                                    FetchIncrement(i + 1, j + 1, dest)
                                Case Southwest
                                    FetchIncrement(i - 1, j + 1, dest)
                                Case North
                                    FetchIncrement(i, j - 1, dest)
                                Case South
                                    FetchIncrement(i, j + 1, dest)
                                Case East
                                    FetchIncrement(i + 1, j, dest)
                                Case West
                                    FetchIncrement(i - 1, j, dest)
                            End Select
                        End If
                    End If
                Next j
            Next i
        Next k
    End Sub


    ' This macro is called several times per square by getrain(), below.  It
    ' simply tests the square for several conditions: if the square is on the
    ' heat equator, itcz is set to one; if the wind blows south in this square,
    ' it is on the flank of a circular wind zone (and thus less rainy); the local
    ' rain sum, x, is increased according to the fetch sum in the square.

    Private Function RainTest(ByRef X As Integer, ByRef buf As Integer, ByRef xx As Integer, ByRef yy As Integer, ByRef itcz As Integer, ByRef flank As Integer) As Integer
        If (pr(buf, xx, yy) = modTecGlobals.Pressure_E.PR_HEQ) Then itcz = 1
        If (wd(buf, xx, yy) And South) Then flank = 1
        X = X + (fs(xx, yy) * mvarNearFlankDelta)
        RainTest = X
    End Function


    Private Sub GetRain(ByRef buf As Integer)
        '   Once the fetch array is computed, this function looks at each square to
        '   determine the amount of rainfall there.  The above macro is called five
        '   times, once for the square and each of its four neighbors; this determines
        '   whether the square is near the ITCZ or the flank of an air cycle.  The
        '   sum of fetches for the neighbors is also determined.   Finally, each of the
        '   factors is weighted and added to the rainfall value:  the local fetch value,
        '   a land factor, the nearness of the heat equator, and the nearness of a
        '   flank.  Note that while rn is zeroed in getfetch(), it may be increased by
        '   rain falling on mountains, so it is nonzero when this function is called.

        Dim itcz, j, i, X, flank As Integer

        For i = 0 To mvarSmallXSize
            For j = 0 To mvarSmallYSize
                flank = 0
                itcz = 0
                X = rn(buf, i, j)

                If (i < mvarSmallXSize - 1) Then
                    X = RainTest(X, buf, i + 1, j, itcz, flank)
                Else
                    X = RainTest(X, buf, 0, j, itcz, flank)
                End If
                If i <> 0 Then
                    X = RainTest(X, buf, i - 1, j, itcz, flank)
                Else
                    X = RainTest(X, buf, mvarSmallXSize - 1, j, itcz, flank)
                End If
                If (j < mvarSmallYSize - 1) Then X = RainTest(X, buf, i, j + 1, itcz, flank)

                If (j) Then X = RainTest(X, buf, i, j - 1, itcz, flank)

                X = RainTest(X, buf, i, j, itcz, flank)

                X = X + (mvarRainConst + mvarFetchDelta * fs(i, j))
                If (L(i, j)) Then X = X + mvarLandDelta
                If (pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HEQ) Then X = X + mvarHeatEquatorDelta
                If itcz <> 0 Then X = X + mvarNearHeatEquatorDelta
                If flank <> 0 Then X = X + mvarFlankDelta
                If X < 0 Then X = 0

                If X > MaxRain Then MaxRain = X
                If X < MinRain Then MinRain = X

                rn(buf, i, j) = X

            Next j
        Next i



    End Sub


    '' WIND



    Private Sub ComputeWind()
        '   This is the main function in this file; it calls getpress() to create
        '   a smoothed pressure map, then getwind() to put isobars (wind lines) on
        '   the output map.  The last step makes sure that contradictory winds are
        '   removed, such as N and S winds in the same square.

        Dim X, i, j, buf As Integer

        For buf = 0 To mvarBSize
            status(modTecGlobals.MainType_E.M_WIND, buf)
            GetPressure(buf)
            GetWind(buf)
            For j = 0 To mvarSmallYSize
                For i = 0 To mvarSmallXSize
                    X = wd(buf, i, j)
                    If (X And North) Then
                        X = X And (Not South)
                        If (X And East) Then X = X And (Not West)
                    End If
                    wd(buf, i, j) = X
                Next i
            Next j
            System.Windows.Forms.Application.DoEvents()
        Next buf


    End Sub



    Private Sub GetPressure(ByRef buf As Integer)
        '   This function takes the high and low markings from pressure.c and creates
        '   a smoothed function.  Highs turn into MAXPRESS and lows turn into 0.

        Dim i, j As Integer

        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                ' Zero out the arrays to be used
                wd(buf, i, j) = 0
                hl(0, i, j) = 0
                hl(1, i, j) = 0

                ' Fill hl(0) with the low pressure zones, and hl(1) with highs
                If (pr(buf, i, j) = modTecGlobals.Pressure_E.PR_LOW) Then
                    hl(0, i, j) = -1
                ElseIf (pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HIGH) Then
                    hl(1, i, j) = -1
                ElseIf (pr(buf, i, j) = modTecGlobals.Pressure_E.PR_HEQ) Then
                    hl(0, i, j) = -1
                End If
            Next i
        Next j

        ' Set each square in hl(0) to the distance from that square to the
        ' nearest low, and each square in hl(1) to the distance to a high.
        range3D(hl, 0)
        range3D(hl, 1)

        ' The final pressure, in array p, is zero if a low is there and
        ' MAXPRESS if a high is there.  Otherwise, the pressure in a square is
        ' proportional to the ratio of (distance from the square to the nearest
        ' low) to (total of distance from nearest high and nearest low).  This
        ' gives a smooth curve between the extremes.
        For j = 0 To mvarSmallYSize
            For i = 0 To mvarSmallXSize
                If (hl(1, i, j) = -1) Then
                    p(i, j) = MAXPRESS
                ElseIf (hl(0, i, j) = -1) Then
                    p(i, j) = 0
                Else
                    p(i, j) = (MAXPRESS * hl(0, i, j)) / (hl(0, i, j) + hl(1, i, j))
                End If
            Next i
        Next j
    End Sub


    Private Sub GetWind(ByRef buf As Integer)
        '   This function draws isobars around the pressure map created above.  These
        '   isobars are the directions of wind flow.  The isobars are given a direction
        '   depending on whether the square is above or below the heat equator; north of
        '   the heat equator, the winds blow counterclockwise out from a low, while
        '   south of it, the opposite is true.

        Dim e, a, i, j, b, bar As Integer

        ' Step from 0 to MAXPRESS by BARSEP; bar is the pressure for which this
        ' isobar will be drawn.
        For bar = BarSep To MAXPRESS Step BarSep
            For i = 0 To mvarSmallXSize
                e = 0
                For j = 0 To mvarSmallYSize

                    ' Set e if this square is south of the heat equator
                    a = p(i, j)
                    If (pr(buf, i, j) = 3) Then e = 1

                    ' Provided the square is not at the top of the array, compare the
                    ' pressure here to the pressure one square up.  This gives the
                    ' direction of the wind in terms of east / west flow.
                    If j <> 0 Then
                        b = p(i, j - 1)
                        If ((a < bar) And (b >= bar)) Then
                            If e <> 0 Then
                                wd(buf, i, j) = wd(buf, i, j) Or East
                            Else
                                wd(buf, i, j) = wd(buf, i, j) Or West
                            End If
                        End If
                        If ((a >= bar) And (b < bar)) Then
                            If e <> 0 Then
                                wd(buf, i, j) = wd(buf, i, j) Or West
                            Else
                                wd(buf, i, j) = wd(buf, i, j) Or East
                            End If
                        End If
                    End If

                    ' Compare the pressure here to the pressure one square to the left
                    ' (including wraparound); this gives the wind direction in terms
                    ' of north / south flow.
                    If i <> 0 Then
                        b = p(i - 1, j)
                    Else
                        b = p(mvarSmallXSize - 1, j)
                    End If
                    If ((a < bar) And (b >= bar)) Then
                        If e <> 0 Then
                            wd(buf, i, j) = wd(buf, i, j) Or North
                        Else
                            wd(buf, i, j) = wd(buf, i, j) Or South
                        End If
                    End If
                    If ((a >= bar) And (b < bar)) Then
                        If e <> 0 Then
                            wd(buf, i, j) = wd(buf, i, j) Or South
                        Else
                            wd(buf, i, j) = wd(buf, i, j) Or North
                        End If
                    End If
                Next j
            Next i
        Next bar
    End Sub


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mvarXSize = 0
        mvarYSize = 0
        mvarBSize = MAXB

        mvarZShelf = 8
        mvarZCoast = 16

        assign_colors(mvarZShelf, mvarZCoast)

        ReDim L(mvarXSize, mvarYSize)
        ReDim lm(mvarXSize, mvarYSize)
        ReDim rn(mvarBSize, mvarXSize, mvarYSize)
        ReDim tt(mvarBSize, mvarXSize, mvarYSize)
        ReDim ts(mvarBSize, mvarXSize, mvarYSize)
        ReDim wd(mvarBSize, mvarXSize, mvarYSize)
        ReDim pr(mvarBSize, mvarXSize, mvarYSize)
        ReDim rn(mvarBSize, mvarXSize, mvarYSize)
        ReDim fr(2, mvarXSize, mvarYSize)
        ReDim fs(mvarXSize, mvarYSize)
        ReDim pm(mvarBSize, mvarXSize, mvarYSize)
        ReDim r(mvarXSize, mvarYSize)
        ReDim p(mvarXSize, mvarYSize)
        ReDim hl(2, mvarXSize, mvarYSize)
        ReDim cl(mvarXSize, mvarYSize)

        mvarMaxRange = 15
        mvarMaxStep = 10000

        mvarZCoast = 16
        mvarZShelf = 8

        mvarMaxRange = 15

        TEMPSCALE = 10

        ReDim change(5)
        change(0) = 1
        change(1) = 1
        change(2) = 1
        change(3) = 1
        change(4) = 1
        change(5) = 1
        step_Renamed = 0

        TCSIZE = 4
        RCSIZE = 5
        mvarIcebergK = 263.0#

        ' This array is the heart of the climate routine; temperature increases
        ' going down the array, and rainfall increases going from left to right.





        ' Heat.c
        mvarTilt = 23.0#
        mvarEccentricity = 0.0167
        mvarEccPhase = PI
        mvarLandTemperatureRange = 74.0#
        mvarLandTemperatureMean = 285.0#
        mvarOceanTemperatureRange = 50.0#
        mvarOceanTemperatureMean = 285.0#

        ' Pressure.c
        mvarOOThresh = 11
        mvarOLThresh = 2
        mvarLOThresh = 6
        mvarLLThresh = 15
        mvarOHMin = 130
        mvarOHMax = 180
        mvarOLMin = 40
        mvarOLMax = 65
        mvarLHMin = 0
        mvarLHMax = 20
        mvarLLMin = 220
        mvarLLMax = 255

        ' rain.c
        mvarMaxFetch = 11
        mvarRainConst = 32
        mvarLandDelta = -10
        mvarMountainDelta = 32
        mvarFetchDelta = 4
        mvarNearFlankDelta = -3
        mvarHeatEquatorDelta = 32
        mvarNearHeatEquatorDelta = 24
        mvarFlankDelta = -24

        'wind.c

        mvarBarSep = 16

    End Sub


    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


        MAXMOUNTAINHEIGHT = 327
    End Sub


    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        ReDim L(0, 0)
        ReDim lm(0, 0)
        ReDim rn(0, 0, 0)
        ReDim tt(0, 0, 0)
        ReDim ts(0, 0, 0)
        ReDim wd(0, 0, 0)
        ReDim pr(0, 0, 0)
        ReDim rn(0, 0, 0)
        ReDim fr(0, 0, 0)
        ReDim fs(0, 0)
        ReDim pm(0, 0, 0)
        ReDim hl(0, 0, 0)
        ReDim p(0, 0)
        ReDim r(0, 0)
        ReDim cl(0, 0)

    End Sub


    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub

    Private Function CheckForDnDHills(ByRef i As Integer, ByRef j As Integer) As Boolean

        CheckForDnDHills = False
        If L(i, j) >= 2000 And L(i, j) <= 4000 Then
            CheckForDnDHills = True
        ElseIf L(i, j) < 2000 And i <= UBound(mvarGradientMagnitude, 1) And j <= UBound(mvarGradientMagnitude, 2) Then
            If mvarGradientMagnitude(i, j) >= 2000 Then CheckForDnDHills = True
        End If
    End Function


End Class