Option Strict Off
Option Explicit On

Imports Microsoft.VisualBasic.Compatibility

<System.Runtime.InteropServices.ComVisible(False)> Friend Class CLogAndIniRoutines
    '****************************************************************************************
    Private Const ClassName As String = "CLogAndIniRoutines"
    Private Const Description As String = "Common routines for writing log files and reading ini files."
    '   Description:
    '   Implemented Interfaces:
    '   Referenced Components:
    '
    '   Public Properties:
    '       Name                    Access    Description
    '       -------------------     ------    ---------------------------------------------
    '       bWriteLog Boolean If FALSE, writeLogEntry only displays the message in the debug window.
    '       strCommentCharacter  String The character used to denote comments in the ini file. For the OSMD-CAS, this value is "#".
    '       strIniFileName  String  The name of the ini file.
    '       strLogFileName  String  The name of the log file.
    '
    '   Public Methods:
    '       Name                    Argument    Description
    '       -------------------     --------    ---------------------------------------------
    '       extractValueFromIniFile strSearch   This searches the Ini file for "strSearch=" and returns all characters on that line until a comment character is reached.
    '       FileExist               strFileName Returns TRUE if the file exists.
    '       writeLogEntry           strMessage  Appends strMessage to the end of the Log file.
    '
    '   History:
    '       Date            Init.   Version/Rev Description
    '       -----------     -----   ----------- ---------------------------------------------
    '
    '****************************************************************************************
    ' Change History
    '----------------------------------------------------------------------------------------
    ' Change ID:
    ' Initials:
    ' Func/Proc:
    ' Description:
    ' Date:
    ' Version/Rev:

    'local variable(s) to hold property value(s)
    Private mvarstrIniFileName As String 'local copy
    Private mvarstrLogFileName As String 'local copy
    Private mvarstrCommentCharacter As String 'local copy
    Private mvarbWriteLog As Boolean 'local copy




    Public Property bWriteLog() As Boolean
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.bWriteLog
            bWriteLog = mvarbWriteLog
        End Get
        Set(ByVal Value As Boolean)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.bWriteLog = 5
            mvarbWriteLog = Value
        End Set
    End Property







    Public Property strCommentCharacter() As String
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.strCommentCharacter
            strCommentCharacter = mvarstrCommentCharacter
        End Get
        Set(ByVal Value As String)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.strCommentCharacter = 5
            mvarstrCommentCharacter = Value
        End Set
    End Property







    Public Property strLogFileName() As String
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.strLogFileName
            strLogFileName = mvarstrLogFileName
        End Get
        Set(ByVal Value As String)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.strLogFileName = 5
            mvarstrLogFileName = Value
        End Set
    End Property





    Public Property strIniFileName() As String
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.strIniFileName
            strIniFileName = mvarstrIniFileName
        End Get
        Set(ByVal Value As String)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.strIniFileName = 5
            mvarstrIniFileName = Value
        End Set
    End Property


    Public Function FileExist(ByVal strFileName As String) As Boolean

        On Error GoTo errDoesFileExist

        'check to see if the file exist in the specified directory
        'first check to see if strFileName is just a directory.
        'directories will return true
        If Mid(strFileName, Len(strFileName)) = "\" Then
            'this is just a directory
            FileExist = False
            Exit Function
        End If

        'then check to see if the file exists.
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        FileExist = IIf(Dir(strFileName) <> "", True, False)

        Exit Function

errDoesFileExist:

    End Function

    Public Function DirExist(ByVal strFileName As String) As Boolean

        On Error GoTo errDoesFileExist

        'check to see if the file exist in the specified directory
        'first check to see if strFileName is just a directory.
        'directories will return true

        'then check to see if the file exists.
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        DirExist = IIf(Dir(strFileName, FileAttribute.Directory) <> "", True, False)

        Exit Function

errDoesFileExist:
    End Function

    Public Sub writeLogEntry(ByRef strMessage As String)
        ' displays a message  in teh immediate pane, or adds it to the log file
        Dim ts As Object
        Dim fso As Object
        If mvarbWriteLog = True Then

            fso = CreateObject("Scripting.FileSystemObject")
            'UPGRADE_WARNING: Couldn't resolve default property of object fso.OpenTextFile. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ts = fso.OpenTextFile(mvarstrLogFileName, 8, True)

            ' write the line to the log file
            'UPGRADE_WARNING: Couldn't resolve default property of object ts.WriteLine. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ts.WriteLine(strMessage)
            'UPGRADE_WARNING: Couldn't resolve default property of object ts.Close. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ts.Close()
        Else
            Debug.Print(strMessage)
        End If
    End Sub

    Private Function ParseIniFileString(ByRef strIniLine As String, ByRef strVariableName As String) As String
        ' extracts the value as a string from the ini file string of the format _
        '" variablename=value # comment "
        Dim offset, strDiff As Integer
        Dim commentOffset As Integer
        Dim strIniLine2, strVariableName2 As String

        strIniLine2 = Trim(strIniLine)
        strVariableName2 = Trim(strVariableName)
        offset = Len(strVariableName2) + 1
        strDiff = StrComp(Left(strIniLine2, Len(strVariableName2)), strVariableName2)
        If strDiff = 0 Then
            commentOffset = InStr(offset, strIniLine2, mvarstrCommentCharacter)
            If commentOffset > 0 Then
                ParseIniFileString = Trim(Mid(strIniLine2, offset + 1, commentOffset - offset - 2))
            Else
                ParseIniFileString = Trim(Mid(strIniLine2, offset, Len(strIniLine2) - offset + 1))
            End If
        Else
            ParseIniFileString = ""
        End If
    End Function

    Public Function extractValueFromIniFile(ByVal strSearch As String) As String

        Dim fso, ts As Object
        Dim strBasePath As String

        ' append an "=" sign if it's not present
        If Right(strSearch, 1) <> "=" Then
            strSearch = strSearch & "="
        End If

        ' searches the osmd-cas.ini file and returns the value
        If FileExist(mvarstrIniFileName) = True Then
            fso = CreateObject("Scripting.FileSystemObject")

            ' read the computer name
            'UPGRADE_WARNING: Couldn't resolve default property of object fso.OpenTextFile. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ts = fso.OpenTextFile(mvarstrIniFileName)
            Do
                'UPGRADE_WARNING: Couldn't resolve default property of object ts.Readline. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                strBasePath = ts.Readline
                'UPGRADE_WARNING: Couldn't resolve default property of object ts.AtEndOfStream. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Loop Until StrComp(UCase(Left(strBasePath, Len(strSearch))), UCase(strSearch)) = 0 Or ts.AtEndOfStream = True
            extractValueFromIniFile = ParseIniFileString(strBasePath, strSearch)
            'UPGRADE_WARNING: Couldn't resolve default property of object ts.Close. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ts.Close()

            ' remove any comments

            ' read the log flag
            'UPGRADE_NOTE: Object ts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            ts = Nothing
        Else
            Debug.Print(VB6.TabLayout("File " & mvarstrIniFileName & " is missing.", MsgBoxStyle.Critical))
            extractValueFromIniFile = ""
        End If
        'UPGRADE_NOTE: Object fso may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        fso = Nothing
    End Function


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mvarbWriteLog = False
        mvarstrCommentCharacter = "#"
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
End Class