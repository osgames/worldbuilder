VERSION 5.00
Begin VB.Form frmDemographics 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kingdom Demographics"
   ClientHeight    =   6750
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9675
   Icon            =   "frmDemographics.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6750
   ScaleWidth      =   9675
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Cities and Towns"
      Height          =   6615
      Left            =   5100
      TabIndex        =   47
      Top             =   60
      Width           =   4515
      Begin VB.ListBox lstVillages 
         Height          =   3375
         Left            =   2340
         TabIndex        =   50
         Top             =   2940
         Width           =   1935
      End
      Begin VB.ListBox lstTowns 
         Height          =   3375
         Left            =   120
         TabIndex        =   49
         Top             =   2940
         Width           =   1935
      End
      Begin VB.ListBox lstCities 
         Height          =   2595
         Left            =   120
         TabIndex        =   48
         Top             =   240
         Width           =   4215
      End
   End
   Begin VB.TextBox acastles 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   40
      Top             =   6360
      Width           =   1095
   End
   Begin VB.TextBox rcastles 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   39
      Top             =   6060
      Width           =   1095
   End
   Begin VB.TextBox dcities 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   38
      Top             =   5760
      Width           =   1095
   End
   Begin VB.TextBox dtowns 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   37
      Top             =   5460
      Width           =   1095
   End
   Begin VB.TextBox dvillage 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   36
      Top             =   5160
      Width           =   1095
   End
   Begin VB.TextBox farmland 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   35
      Top             =   4860
      Width           =   1095
   End
   Begin VB.TextBox citynum 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   34
      Top             =   4560
      Width           =   1095
   End
   Begin VB.TextBox lcitypop3 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   33
      Top             =   4260
      Width           =   1095
   End
   Begin VB.TextBox lcitypop2 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   32
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox lcitypop 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   31
      Top             =   3660
      Width           =   1095
   End
   Begin VB.TextBox citypop 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   30
      Top             =   3360
      Width           =   1395
   End
   Begin VB.TextBox wandpop 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   29
      Top             =   3060
      Width           =   1395
   End
   Begin VB.TextBox towns 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   28
      Top             =   2760
      Width           =   1395
   End
   Begin VB.TextBox townpop 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   27
      Top             =   2460
      Width           =   1395
   End
   Begin VB.TextBox villages 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   26
      Top             =   2160
      Width           =   1395
   End
   Begin VB.TextBox villagepop 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   25
      Top             =   1860
      Width           =   1395
   End
   Begin VB.TextBox population 
      Enabled         =   0   'False
      Height          =   255
      Left            =   2640
      TabIndex        =   24
      Top             =   1560
      Width           =   1395
   End
   Begin VB.TextBox age 
      Height          =   285
      Left            =   2640
      TabIndex        =   6
      Text            =   "300"
      Top             =   660
      Width           =   1335
   End
   Begin VB.TextBox density 
      Height          =   285
      Left            =   2640
      TabIndex        =   5
      Text            =   "71"
      Top             =   360
      Width           =   1335
   End
   Begin VB.TextBox kingarea 
      Height          =   285
      Left            =   2640
      TabIndex        =   4
      Text            =   "70000"
      Top             =   60
      Width           =   1335
   End
   Begin VB.CommandButton cmdCalculate 
      Caption         =   "Calculate Kingdom Details"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   3615
   End
   Begin VB.Label lblDistricts3 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3780
      TabIndex        =   46
      Top             =   4260
      Width           =   495
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "Districts"
      Height          =   195
      Left            =   4320
      TabIndex        =   45
      Top             =   4290
      Width           =   555
   End
   Begin VB.Label lblDistricts2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3780
      TabIndex        =   44
      Top             =   3960
      Width           =   495
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Districts"
      Height          =   195
      Left            =   4320
      TabIndex        =   43
      Top             =   3990
      Width           =   555
   End
   Begin VB.Label lblDistricts 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   3780
      TabIndex        =   42
      Top             =   3660
      Width           =   495
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Districts"
      Height          =   195
      Left            =   4320
      TabIndex        =   41
      Top             =   3690
      Width           =   555
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Number of active castles:"
      Height          =   195
      Index           =   16
      Left            =   120
      TabIndex        =   23
      Top             =   6390
      Width           =   1800
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Number of ruin sites:"
      Height          =   195
      Index           =   15
      Left            =   120
      TabIndex        =   22
      Top             =   6090
      Width           =   1440
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Avg distance between cities:"
      Height          =   195
      Index           =   14
      Left            =   120
      TabIndex        =   21
      Top             =   5790
      Width           =   2040
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Avg Distance between towns:"
      Height          =   195
      Index           =   13
      Left            =   120
      TabIndex        =   20
      Top             =   5490
      Width           =   2130
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Avg Distance between villages:"
      Height          =   195
      Index           =   12
      Left            =   120
      TabIndex        =   19
      Top             =   5190
      Width           =   2235
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Area of farmland:"
      Height          =   195
      Index           =   11
      Left            =   120
      TabIndex        =   18
      Top             =   4890
      Width           =   1200
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Number of other cities:"
      Height          =   195
      Index           =   10
      Left            =   120
      TabIndex        =   17
      Top             =   4590
      Width           =   1590
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Population of Third Largest City:"
      Height          =   195
      Index           =   9
      Left            =   120
      TabIndex        =   16
      Top             =   4290
      Width           =   2250
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Population of Second Largest City:"
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   15
      Top             =   3990
      Width           =   2445
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Population of Largest City:"
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   14
      Top             =   3690
      Width           =   1845
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "City Population:"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   13
      Top             =   3390
      Width           =   1095
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Wandering Population:"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   12
      Top             =   3090
      Width           =   1620
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Number of Towns in Kingdom:"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   11
      Top             =   2790
      Width           =   2130
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Population Living in Towns:"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   10
      Top             =   2490
      Width           =   1950
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Number of Villages in Kingdom:"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   9
      Top             =   2190
      Width           =   2190
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Population Living in Villages:"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   1890
      Width           =   2010
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Kingdom Population:"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   1590
      Width           =   1455
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Age of Kingdom:"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   705
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Population Density:"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   405
      Width           =   1365
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Kingdom Area:"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   105
      Width           =   1035
   End
End
Attribute VB_Name = "frmDemographics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub cmdCalculate_Click()
  
    Dim karea As Single
    Dim kdensity As Single
    Dim kage As Single
    Dim pop As Single
    Dim vpop As Single
    Dim kvillages As Single
    Dim tpop As Single
    Dim ktowns As Single
    Dim kwandpop As Single
    Dim kcitypop As Single
    Dim sqpop As Single
    Dim klcitypop As Single
    Dim klcitypop2 As Single
    Dim klcitypop3 As Single
    Dim kcitynum As Single
    Dim kfarmland As Single
    Dim kdvillage As Single
    Dim kdtowns As Single
    Dim kdcities As Single
    Dim krcastles As Single
    Dim kacastles As Single
    Dim farmarea As Single
    Dim lremainpop As Long
    Dim lngcitypop As Long
    
    cmdCalculate.Enabled = False
  
    Set frmTec.Cities = New CCities
  
    karea = kingarea
    kdensity = density
    kage = age
    
    If karea < 1 Then
      MsgBox "Kingdom Area must be greater than 0."
      Exit Sub
    End If

    If kdensity < 0 Then
      MsgBox "Population Density must be greater than 0"
      Exit Sub
    End If
    
    If kdensity > 1000 Then
      MsgBox "Population Density must be less than 1001"
      Exit Sub
    End If
    
    If kage < 1 Then
      MsgBox "Kingdom Age must be greater than 0."
      Exit Sub
    End If
  
    pop = FormatNumber(karea * kdensity, 0)
    vpop = FormatNumber(pop * 0.89, 0)
    kvillages = FormatNumber(vpop / 700, 0)
    tpop = FormatNumber(pop * 0.06, 0)
    ktowns = FormatNumber(tpop / 5000, 0)
    kwandpop = FormatNumber(pop * 0.02, 0)
    kcitypop = FormatNumber(pop * 0.03, 0)
    sqpop = pop ^ 0.5
    klcitypop = FormatNumber(sqpop * (Rnd * 4 + Rnd * 4 + 10), 0)
    klcitypop2 = FormatNumber((0.25 + Rnd * 0.5) * klcitypop, 0)
    klcitypop3 = FormatNumber((Rnd * 0.1 + 0.8) * klcitypop2, 0)
    
    lstCities.Clear
    If gcblnOGL = False Then
      lstCities.AddItem klcitypop & Chr(9) & " (" & CStr(CLng(klcitypop / 500)) & " districts)"
      lstCities.AddItem klcitypop2 & Chr(9) & " (" & CStr(CLng(klcitypop2 / 500)) & " districts)"
      lstCities.AddItem klcitypop3 & Chr(9) & " (" & CStr(CLng(klcitypop3 / 500)) & " districts)"
    Else
      lstCities.AddItem klcitypop
      lstCities.AddItem klcitypop2
      lstCities.AddItem klcitypop3
    End If
    
    kcitynum = 0
    lremainpop = (kcitypop - klcitypop - klcitypop2 - klcitypop3)
    lngcitypop = klcitypop3
    
    frmTec.Cities.Add
    frmTec.Cities(frmTec.Cities.Count).population = klcitypop
    frmTec.Cities.Add
    frmTec.Cities(frmTec.Cities.Count).population = klcitypop2
    frmTec.Cities.Add
    frmTec.Cities(frmTec.Cities.Count).population = klcitypop3
    
    
    Do Until lngcitypop < 11000 Or lremainpop < 12000
      kcitynum = kcitynum + 1
      lngcitypop = (1 - (Rnd * 0.3 + 0.1)) * lngcitypop
      If gcblnOGL = True Then
        lstCities.AddItem lngcitypop
      Else
        lstCities.AddItem lngcitypop & Chr(9) & " (" & CStr(CLng(lngcitypop / 500)) & " districts)"
      End If
      
      frmTec.Cities.Add
      frmTec.Cities(frmTec.Cities.Count).population = lngcitypop
      
      lremainpop = lremainpop - lngcitypop
    Loop
    
    If kcitynum < 0 Then kcitynum = 0
    farmarea = FormatNumber((karea * kdensity) / 180, 0)  ' area
    kfarmland = FormatNumber((farmarea / karea) * 100, 0) ' percentage
    
    
    ktowns = 0
    lremainpop = tpop
    lstTowns.Clear
    Do Until lremainpop < 1700
      ktowns = ktowns + 1
      lngcitypop = -1
      Do Until lngcitypop > 0
        lngcitypop = 1000 + (Rnd * 7000)
      Loop
      'lngcitypop = (Rnd * 0.1 + 0.8) * lngcitypop
      
      frmTec.Cities.Add
      frmTec.Cities(frmTec.Cities.Count).population = lngcitypop
      
      lstTowns.AddItem lngcitypop
      lremainpop = lremainpop - lngcitypop
    Loop
   
    kvillages = 0
    lremainpop = vpop
    lstVillages.Clear
    Do Until lremainpop < 20
      kvillages = kvillages + 1
      lngcitypop = Rnd * 980 + 20
      lstVillages.AddItem lngcitypop
      
      frmTec.Cities.Add
      frmTec.Cities(frmTec.Cities.Count).population = lngcitypop
      
      lremainpop = lremainpop - lngcitypop
    Loop
   
    ' Use farm area for towns and villages since they will depend on a great city.
    If kvillages > 0 Then
      kdvillage = FormatNumber(2 * (farmarea / kvillages) ^ 0.5, 1)
      'kdvillage = FormatNumber(2 * Sqr(karea / kvillages), 1)
    End If
    If ktowns > 0 Then
      kdtowns = FormatNumber(2 * (farmarea / ktowns) ^ 0.5, 1)
      'kdtowns = FormatNumber(2 * Sqr(karea / ktowns), 1)
    End If
    
    ' Use total area for cities since they are independent.
    If kcitynum > 0 Then
      'kdcities = FormatNumber((farmarea / (kcitynum + 3)) ^ 0.5, 1)
      kdcities = FormatNumber(2 * (karea / (kcitynum + 3)) ^ 0.5, 1)
    End If
    krcastles = FormatNumber((pop / 5000000) * (kage ^ 0.5), 0)
    kacastles = FormatNumber(pop / 50000, 0)
    
    population = Format(pop, "###,###,##0")
    villagepop = Format(vpop, "###,###,##0")
    villages = Format(kvillages, "###,###,##0")
    townpop = Format(tpop, "###,###,##0")
    towns = Format(ktowns, "###,###,##0")
    wandpop = Format(kwandpop, "###,###,##0")
    citypop = Format(kcitypop, "###,###,##0")
    lcitypop = Format(klcitypop, "###,###,##0")
    lblDistricts = Format(klcitypop / 500, "##0")
    lcitypop2 = Format(klcitypop2, "###,###,##0")
    lblDistricts2 = Format(klcitypop2 / 500, "##0")
    lcitypop3 = Format(klcitypop3, "###,###,##0")
    lblDistricts3 = Format(klcitypop3 / 500, "##0")
    citynum = Format(kcitynum, "###,###,##0")
    farmland = Format(kfarmland / 100, "##0.0#%")
    dvillage = Format(kdvillage, "###,###,##0")
    dtowns = Format(kdtowns, "###,###,##0")
    dcities = Format(kdcities, "###,###,##0")
    rcastles = Format(krcastles, "###,###,##0")
    acastles = Format(kacastles, "###,###,##0")
    
    cmdCalculate.Enabled = True
    
End Sub

Private Sub Form_Load()
  If gcblnOGL = True Then
    Label5.Visible = False
    Label6.Visible = False
    Label8.Visible = False
    lblDistricts.Visible = False
    lblDistricts2.Visible = False
    lblDistricts3.Visible = False
  End If
End Sub

Private Sub lstCities_DblClick()
  Dim strs() As String
  Dim str2 As String
  
  If gcblnOGL = False Then
    
    strs = Split(lstCities.list(lstCities.ListIndex), Chr(9))
    str2 = Mid(strs(1), 3, Len(strs(1)) - 12)
    
    frmRandomCity.Show
    frmRandomCity.txtNDistricts.Text = str2
    DoEvents
    frmRandomCity.cmdGenerate_Click
  Else
    frmSV.Show , Me
    frmSV.lblPopulation.Caption = lstCities.list(lstCities.ListIndex)
    DoEvents
    frmSV.CalculateSV
  End If
End Sub

Private Sub lstTowns_DblClick()
  frmSV.Show , Me
  frmSV.lblPopulation.Caption = lstTowns.list(lstTowns.ListIndex)
  DoEvents
  frmSV.CalculateSV
End Sub

Private Sub lstVillages_DblClick()
  frmSV.Show , Me
  frmSV.lblPopulation.Caption = lstVillages.list(lstVillages.ListIndex)
  DoEvents
  frmSV.CalculateSV
End Sub
