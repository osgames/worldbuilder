VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTec 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Plate Tectonics Simulator (48 mi. resolution)"
   ClientHeight    =   9525
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   16890
   Icon            =   "frmTec.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9525
   ScaleWidth      =   16890
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPopulate 
      Caption         =   "Populate"
      Enabled         =   0   'False
      Height          =   315
      Left            =   12900
      TabIndex        =   31
      Top             =   8460
      Width           =   855
   End
   Begin VB.ComboBox cboRegionCol 
      Height          =   315
      ItemData        =   "frmTec.frx":1BB2
      Left            =   14580
      List            =   "frmTec.frx":1BE6
      Style           =   2  'Dropdown List
      TabIndex        =   30
      Top             =   8460
      Width           =   735
   End
   Begin VB.ComboBox cboRegionRow 
      Height          =   315
      ItemData        =   "frmTec.frx":1C20
      Left            =   13860
      List            =   "frmTec.frx":1C3C
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   8460
      Width           =   735
   End
   Begin VB.CommandButton cmdGetRegion 
      Caption         =   "Select region"
      Enabled         =   0   'False
      Height          =   315
      Left            =   15480
      TabIndex        =   28
      Top             =   8460
      Width           =   1335
   End
   Begin VB.CommandButton cmdShowParameters 
      Caption         =   "Show Parameters"
      Height          =   315
      Left            =   8640
      TabIndex        =   27
      Top             =   8460
      Width           =   1515
   End
   Begin VB.CommandButton cmdClimateRun 
      Caption         =   "Run Climate"
      Enabled         =   0   'False
      Height          =   315
      Left            =   11700
      TabIndex        =   9
      Top             =   8460
      Width           =   1155
   End
   Begin VB.ComboBox cboSize 
      Height          =   315
      ItemData        =   "frmTec.frx":1C58
      Left            =   6120
      List            =   "frmTec.frx":1C6B
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   8460
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      Height          =   315
      Left            =   4440
      ScaleHeight     =   255
      ScaleWidth      =   12315
      TabIndex        =   7
      Top             =   8880
      Width           =   12375
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   9270
      Width           =   16890
      _ExtentX        =   29792
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
            Text            =   "Min. temperature:"
            TextSave        =   "Min. temperature:"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
            Text            =   "Max. temperature:"
            TextSave        =   "Max. temperature:"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
            Text            =   "Max. Altitude:"
            TextSave        =   "Max. Altitude:"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
            Text            =   "Min Rainfall:"
            TextSave        =   "Min Rainfall:"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4921
            Text            =   "Max Rainfall:"
            TextSave        =   "Max Rainfall:"
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox cboSeason 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmTec.frx":1CD8
      Left            =   2280
      List            =   "frmTec.frx":1D00
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   8880
      Width           =   2115
   End
   Begin VB.ComboBox cboDraw 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmTec.frx":1D66
      Left            =   60
      List            =   "frmTec.frx":1D88
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   8880
      Width           =   2235
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Run Tectonics"
      Height          =   315
      Left            =   10200
      TabIndex        =   2
      Top             =   8460
      Width           =   1455
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   2280
      TabIndex        =   1
      Top             =   8460
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   661
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   8205
      Left            =   420
      ScaleHeight     =   8145
      ScaleWidth      =   16350
      TabIndex        =   0
      Top             =   60
      Width           =   16410
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "E"
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   26
      Top             =   7620
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "D"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   25
      Top             =   6600
      Width           =   120
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "C"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   24
      Top             =   5580
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "A"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   23
      Top             =   4560
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "A"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   22
      Top             =   3540
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "C"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   21
      Top             =   2520
      Width           =   105
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "D"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   20
      Top             =   1500
      Width           =   120
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "E"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   19
      Top             =   480
      Width           =   105
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "67� N"
      Height          =   195
      Index           =   5
      Left            =   0
      TabIndex        =   18
      Top             =   1020
      Width           =   405
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "23� N"
      Height          =   195
      Index           =   4
      Left            =   0
      TabIndex        =   17
      Top             =   3000
      Width           =   405
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "45� N"
      Height          =   195
      Index           =   3
      Left            =   0
      TabIndex        =   16
      Top             =   2040
      Width           =   405
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "90� N"
      Height          =   195
      Index           =   1
      Left            =   -15
      TabIndex        =   15
      Top             =   0
      Width           =   405
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "90� S"
      Height          =   195
      Index           =   2
      Left            =   0
      TabIndex        =   14
      Top             =   8100
      Width           =   390
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "67� S"
      Height          =   195
      Index           =   1
      Left            =   0
      TabIndex        =   13
      Top             =   7140
      Width           =   390
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "45� S"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   12
      Top             =   6120
      Width           =   390
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "23� S"
      Height          =   195
      Index           =   0
      Left            =   0
      TabIndex        =   11
      Top             =   5100
      Width           =   390
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "0�"
      Height          =   195
      Left            =   240
      TabIndex        =   10
      Top             =   4080
      Width           =   150
   End
   Begin VB.Label lblStep 
      Caption         =   "0"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   8520
      Width           =   2160
   End
End
Attribute VB_Name = "frmTec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
  
  
Public clim As CClim
Public tec As CTec
Public frac As CCollageFractal
Public Cities As CCities
Public humangeo As CHumanGeography
Dim hiresdata() As Long
Dim t() As Long

Private Sub cboDraw_Click()
  Dim popden() As Single
  Dim i As Long, j As Long
  
  Select Case cboDraw.Text
    Case "Plates"
      tec.platedraw
    Case "Topography"
      tec.DrawTopographicReliefMap
      'frac.DrawHiRes
    Case "Climate"
      clim.DrawClimateMap
    Case "Temperature"
      clim.DrawTemperatureMap cboSeason.ListIndex
    Case "Pressure"
      clim.DrawPressureMap cboSeason.ListIndex
    Case "Wind"
      clim.DrawWindMap cboSeason.ListIndex
    Case "Rainfall"
      clim.DrawPrecipitationMap cboSeason.ListIndex
    Case "True Color"
      clim.DrawTrueColor
    Case "DnD Climate Zones"
      clim.DrawDNDClimateMap
    Case "Population"
      ' temporary
      'If humangeo Is Nothing Then
        Set humangeo = New CHumanGeography
        humangeo.SetClimate clim.GetClimateMap
        humangeo.XSize = clim.XSize
        humangeo.YSize = clim.YSize
        ReDim popden(clim.XSize, clim.YSize) As Single
        For i = 0 To humangeo.XSize
          For j = 0 To humangeo.YSize
            popden(i, j) = Rnd * 255
          Next j
        Next i
        humangeo.SetPopDen popden
      'End If
      humangeo.DrawPopulationMap
  End Select
  DrawGrid
  DrawRegionGrid
End Sub

Private Sub cboSeason_Click()
  cboDraw_Click
End Sub

Private Sub cmdClimateRun_Click()
  
  ' Clim was originally written for 60x30.  We need 128x64.
  Dim dtmStart As Date
  Dim dtmEnd As Date
  
  dtmStart = Now
  
  cmdShowParameters.Enabled = False
  Command1.Enabled = False
  cmdClimateRun.Enabled = False
  cboDraw.Enabled = False
  cboSeason.Enabled = False
  cboSize.Enabled = False

  With clim
    
    .LetDEM frac.GetHiResDEM

    .BSize = 12

    If .BSize = 4 Then
      With cboSeason
        .Clear
        .AddItem "Winter"
        .AddItem "Spring"
        .AddItem "Summer"
        .AddItem "Autumn"
      End With
    ElseIf .BSize = 12 Then
      With cboSeason
        .Clear
        .AddItem "January"
        .AddItem "February"
        .AddItem "March"
        .AddItem "April"
        .AddItem "May"
        .AddItem "June"
        .AddItem "July"
        .AddItem "August"
        .AddItem "September"
        .AddItem "October"
        .AddItem "November"
        .AddItem "December"
      End With
    End If

    .RunModel
  
  End With
  
  dtmEnd = Now
  
  StatusBar1.Panels(1).Text = "Time to complete = " & Format(dtmEnd - dtmStart, "hh:mm:ss")
  
  cmdShowParameters.Enabled = True
  Command1.Enabled = True
  cmdClimateRun.Enabled = True
  cboDraw.Enabled = True
  cboSeason.Enabled = True
  cboSize.Enabled = True

End Sub

Private Sub cmdGetRegion_Click()
  ExtractRegion CByte(cboRegionCol.Text), CByte(cboRegionRow.Text)
End Sub

Private Sub cmdPopulate_Click()
  Dim sngArea As Single
  Dim sngTotalArea As Single
  
  Dim lat As Single
  Dim t() As Integer
  Dim i As Long, j As Long
  
  'sngArea = (1 - tec.HydroPct / 100#) * 4 * PI * (REarth / 1.61) ^ 2
  
  sngTotalArea = 0
  sngArea = 0
  t = tec.GetHeightMap
  For i = 0 To UBound(t, 1)
    lat = PI / 2 - PI * CDbl(j) / CDbl(tec.YSize)
    For j = 0 To UBound(t, 2)
      If t(i, j) >= tec.ZCoast Then sngArea = sngArea + Abs(Cos(lat))
      sngTotalArea = sngTotalArea + Abs(Cos(lat))
    Next j
  Next i
  
  sngArea = (sngArea / sngTotalArea) * (4 * PI * (REarth / 1.61) ^ 2)
  Debug.Print Format(sngArea, "###,###,##0.0") & " sq. mi."
  
  frmDemographics.Show
  With frmDemographics
    .kingarea = Format(sngArea, "0")
    .density = 7 ' same as earth 1340
    .age = 10000 ' rough estimate
    DoEvents
    .cmdCalculate_Click
  End With
End Sub



Private Sub cmdShowParameters_Click()
  frmTecParameters.Show vbModal, Me
End Sub

Private Sub Command1_Click()
  Dim i As Long
  Dim dtmStart As Date
  Dim dtmEnd As Date

  cmdShowParameters.Enabled = False
  Command1.Enabled = False
  cmdClimateRun.Enabled = False
  cboDraw.Enabled = False
  cboSeason.Enabled = False
  cboSize.Enabled = False
  cmdPopulate.Enabled = False
  
  'Command1.Caption = "Cancel"
  MAXX = 1024
  MAXY = 1024
  
  
  dtmStart = Now
  
  tec.XSize = 512
  tec.YSize = 256
  

  n = ((tec.XSize / 2) - 1)
  area = (tec.XSize * tec.XSize)
  


  WINDOW_HEIGHT = SIZE * tec.XSize
  WINDOW_WIDTH = SIZE * tec.XSize
   
  
  Picture1.Cls
  DoEvents
  
  
  'tec.RiftDist = tec.XSize / 20
  With tec
    .DrawEvery = 1
    For i = 0 To MAXPLATE
      .Plates.Add
    Next i
    
    .RunModel tec.MaxStep
  End With
  
  ' interpolate the 512x256 into an 8192x4096, with random bumping
  frac.FractalTerrainInterp tec.GetHeightMap, cboSize.ListIndex
  
  dtmEnd = Now
  
  StatusBar1.Panels(1).Text = "Time to complete = " & Format(dtmEnd - dtmStart, "hh:mm:ss")
  
  cboDraw.Enabled = True
  If cboDraw.ListIndex <> 1 Then
    cboDraw.ListIndex = 1 'cboDraw.ListCount - 1
  Else
    cboDraw_Click
  End If
  
  cboSize.Enabled = True
  Command1.Enabled = True
  cmdClimateRun.Enabled = True
  cmdShowParameters.Enabled = True
  cmdPopulate.Enabled = True
  cmdGetRegion.Enabled = True
End Sub

Private Sub Form_Load()
  Dim i As Long
  Const MAXPLATE = 40
  
  MAXMOUNTAINHEIGHT = 327
  Randomize Timer
  TRandomInit Timer
  
  Me.Show
  cboSize.ListIndex = 0
  
  XWinScale = frmTec.Picture1.ScaleWidth
  YWinScale = frmTec.Picture1.ScaleHeight

  Set tec = New CTec
  Set frac = New CCollageFractal
  Set clim = New CClim
  'DoEvents
  
  tec.BendEvery = 6
  tec.HydroPct = 75
  tec.MaxStep = 25
  clim.OceanTemperatureMean = 285
  clim.OceanTemperatureRange = 50
  clim.LandTemperatureMean = 275
  clim.LandTemperatureRange = 84
  clim.NearFlankDelta = -3
  clim.RainConst = 27
  frmTecParameters.Show vbModal, Me
  
End Sub



Private Function MakeSmallDEM(MainDEM() As Long) As Long()
  Dim SmallDem() As Long
  Dim i As Long, j As Long
  Dim ii As Long, jj As Long
  ReDim SmallDem(128, 64) As Long
    
  For i = 0 To 127
    For j = 0 To 63
      SmallDem(i, j) = 0
      For ii = 0 To 3
        For jj = 0 To 3
          SmallDem(i, j) = SmallDem(i, j) + MainDEM(i * 4 + ii, j * 4 + jj)
        Next jj
      Next ii
      SmallDem(i, j) = SmallDem(i, j) / 16
    Next j
  Next i
  MakeSmallDEM = SmallDem
End Function

Public Function DrawColorBar(lut() As Long)
  Dim i As Long
  Dim stepwidth As Long
  stepwidth = Picture2.ScaleWidth / UBound(lut)
  
  For i = 0 To UBound(lut)
    Picture2.Line (i * stepwidth, 0)-Step(stepwidth, Picture2.ScaleHeight), lut(i), BF
  Next i
  
End Function

Private Sub Form_Unload(Cancel As Integer)
  Set tec = Nothing
  Set frac = Nothing
  Set clim = Nothing
End Sub


Private Function ExtractRegion(row As Byte, col As Byte)
  Dim i As Long
  Dim j As Long
  Dim startrow As Long, startcol As Long
  Dim startrowsmall As Long, startcolsmall As Long
  
  Dim RegionDEM() As Integer
  Dim TempMap() As Integer
  Dim RainMap() As Integer
  Dim PressureMap() As Byte
  Dim WindMap() As Byte
  Dim ClimateMap() As Byte
  
  Dim ContinentDEM() As Integer
  Dim ContinentTempMap() As Integer
  Dim ContinentRainMap() As Integer
  Dim ContinentPressureMap() As Byte
  Dim ContinentWindMap() As Byte
  Dim ContinentClimateMap() As Byte
  
  ' Elevation
  ContinentDEM = tec.GetHeightMap
  
  startrow = row * UBound(ContinentDEM, 1) / 16
  startcol = col * UBound(ContinentDEM, 2) / 8
  
  ReDim RegionDEM(UBound(ContinentDEM, 1) / 16, UBound(ContinentDEM, 1) / 16) As Integer
  For i = 0 To UBound(ContinentDEM, 1) / 16
    For j = 0 To UBound(ContinentDEM, 1) / 16
      RegionDEM(i, j) = ContinentDEM(i + startrow, j + startcol)
    Next j
  Next i
  
  frmRegion.Show , Me
  frmRegion.ZCoast = tec.ZCoast
  frmRegion.ZShelf = tec.ZShelf
  
  ' do random fractal interpolation
  frac.FractalTerrainInterp RegionDEM, 4, True
  
  ' now sculpt mountains
  'frac.CollageTerrain UBound(frac.GetHiResDEM, 1), _
    UBound(frac.GetHiResDEM, 2), 50000, _
    UBound(frac.GetHiResDEM, 1) / 50, MAXMOUNTAINHEIGHT, 0, True
  
  frmRegion.LetDEM frac.GetHiResDEM
  
  ' Temperature

  ' Pressure
  ' Wind
  ' Rainfall
  ' Climate
  
End Function












